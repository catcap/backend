# Generated by Django 4.2 on 2024-03-29 08:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content_handler', '0006_content_img'),
    ]

    operations = [
        migrations.AddField(
            model_name='content',
            name='img_alt',
            field=models.CharField(null=True),
        ),
    ]
