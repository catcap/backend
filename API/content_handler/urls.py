from django.urls import path
from content_handler.views import *

app_name = "content_handler"

urlpatterns = [
    path("get_content/<str:content_type>", PublicContentHandler.as_view(), name="get_content"),
    path("handle_content", ContentHandler.as_view(), name="handle_content"),
    path("handle_content/<str:content_id>", ContentHandler.as_view(), name="edit_content"),
]