from authentication.models import User
from content_handler.models import Content
from django.test import TestCase
from django.urls import reverse
from datetime import datetime, timedelta
import json

class PublicContentHandlerTest(TestCase):
	def setUp(self):
		i = 0
		while i < 10:
			if i % 2 == 0:
				content = Content(type="NEWS", date=datetime.now().date())
			elif i == 9:
				content = Content(type="NEWS", date=(datetime.now().date() + timedelta(days=1)))
			else:
				content = Content(type="DB")
			content.save()
			i+=1

	def test_get_200(self):
		response = self.client.get(reverse("content_handler:get_content", kwargs={"content_type": "all"}))
		data = response.json()["data"]
		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(data), 10)

		response = self.client.get(reverse("content_handler:get_content", kwargs={"content_type": "NEWS"}))
		data = response.json()["data"]
		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(data), 5)

		response = self.client.get(reverse("content_handler:get_content", kwargs={"content_type": "DB"}))
		data = response.json()["data"]
		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(data), 4)

		response = self.client.get(reverse("content_handler:get_content", kwargs={"content_type": "LLT"}))
		data = response.json()["data"]
		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(data), 0)

class ContentHandlerTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True, is_superuser=True)

	def test_post_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.post(reverse("content_handler:handle_content"), {"type": "HP", "title": "POUET"})
		content = Content.objects.get(type="HP")
		self.assertEqual(response.status_code, 200)
		self.assertEqual(content.title, "POUET")

	def test_post_401(self):
		response = self.client.post(reverse("content_handler:handle_content"), {"type": "HP", "title": "POUET"})
		self.assertEqual(response.status_code, 401)
		self.user.is_superuser = False
		self.user.save()
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.post(reverse("content_handler:handle_content"), {"type": "HP", "title": "POUET"})
		self.assertEqual(response.status_code, 401)

	def test_put_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		content = Content(type="HP", title="POUET")
		content.save()
		response = self.client.put(reverse("content_handler:edit_content", kwargs={"content_id": content.id}), {"title": "POUET-POUET"}, content_type="application/json")
		content = Content.objects.get(id=content.id)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(content.title, "POUET-POUET")

	def test_put_404(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.put(reverse("content_handler:edit_content", kwargs={"content_id": "32"}), {"title": "POUET"}, content_type="application/json")
		self.assertEqual(response.status_code, 404)

	def test_put_401(self):
		content = Content(type="HP", title="POUET")
		content.save()
		response = self.client.put(reverse("content_handler:edit_content", kwargs={"content_id": content.id}), {"title": "POUET-POUET"}, content_type="application/json")
		content = Content.objects.get(id=content.id)
		self.assertEqual(response.status_code, 401)
		self.assertEqual(content.title, "POUET")
		self.user.is_superuser = False
		self.user.save()
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.put(reverse("content_handler:edit_content", kwargs={"content_id": content.id}), {"title": "POUET-POUET"}, content_type="application/json")
		content = Content.objects.get(id=content.id)
		self.assertEqual(response.status_code, 401)
		self.assertEqual(content.title, "POUET")
