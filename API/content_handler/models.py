from django.db import models

class Content(models.Model):
    CONTENT_TYPE = [
        ("HP", "HomePage"),
        ("NEWS", "News"),
        ("LLT", "LowLevelTraces"),
        ("HLT", "HighLevelTraces"),
        ("DF", "DataFormating"),
        ("DB", "DataBase"),
        ("MB", "MapBuilding"),
        ("RGPD", "RGPD"),
        ("LN", "LegalNotice")
    ]

    text = models.CharField(max_length=1000, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    video = models.CharField(blank=True, null=True)
    type = models.CharField(choices=CONTENT_TYPE)
    order = models.SmallIntegerField(null=True)
    title = models.CharField(max_length=50, null=True)
    subTitle = models.CharField(max_length=100, null=True)
    img = models.CharField(null=True)
    img_alt = models.CharField(null=True)

    def toJson(self):
        if self.type == "RGPD" or self.type == "LN":
            return {
                "type": self.type,
                "id": self.id,
                "text": self.text,
                "title": self.title,
                "order": self.order,
            }
        elif self.type == "HP":
            return {
                "type": self.type,
                "id": self.id,
                "title": self.title,
                "subTitle": self.subTitle,
            }
        elif self.type == "NEWS":
            return {
                "type": self.type,
                "id": self.id,
                "title": self.title,
                "date": self.date,
                "text": self.text,
                "img": self.img,
                "img_alt": self.img_alt,
            }
        else:
            return {
                "type": self.type,
                "id": self.id,
                "text": self.text,
                "video": self.video,
                "order": self.order,
            }

    def save(self, *args, **kwargs):
        if self.type == "HP" or self.type == "NEWS":
            super().save(*args, **kwargs)
            return
        elif self.type != "HP":
            if self.pk is None:
                max_order_content = Content.objects.filter(type=self.type).order_by('-order').first()
                if max_order_content:
                    max_order = max_order_content.order
                    self.order = max_order + 1
                else:
                    self.order = 1

            else:
                previous_order = Content.objects.get(pk=self.pk).order
                if self.order == previous_order:
                    super().save(*args, **kwargs)
                    return

                if self.order < previous_order:
                    Content.objects.filter(type=self.type, order__gte=self.order, order__lt=previous_order).exclude(pk=self.pk).update(
                        order=models.F('order') + 1
                    )
                else:
                    Content.objects.filter(type=self.type, order__gt=previous_order, order__lte=self.order).exclude(pk=self.pk).update(
                        order=models.F('order') - 1
                    )

            super().save(*args, **kwargs)

    def update(self, **kwargs):
        for attr_name in vars(self):
            if attr_name in kwargs and kwargs[attr_name] is not None:
                setattr(self, attr_name, kwargs[attr_name])
                self.save()

    def delete(self, *args, **kwargs):
        if self.type != "HP" and self.type != "NEWS":
            higher_order_contents = Content.objects.filter(order__gt=self.order)
            for content in higher_order_contents:
                content.order -= 1
                content.save()
            super().delete(*args, **kwargs)
        else:
            super().delete()