from datetime import datetime
from content_handler.models import Content

from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status

from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers

class PublicContentHandler(APIView):
    """ Gets content by type
    :params content_type: one of these values : HP, LLT, HLT, DF, DB, MB or all
    :returns: HTTP_Response 200 with the requested type's content
    """
    my_tags = ["Content"]
    authentication_classes = []
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_description="Gets content by type",
        responses={200: "data"}
    )
    def get(self, request: Request, content_type: str) -> JsonResponse:
        result = []
        if content_type == "all":
            for content in Content.objects.all():
                result.append(content.toJson())
        elif content_type == "NEWS":
            today = datetime.now().date()
            for content in Content.objects.filter(type=content_type, date__lte=today).order_by("-date")[:10]:
                result.append(content.toJson())
        else:
            for content in Content.objects.filter(type=content_type):
                result.append(content.toJson())
        return JsonResponse({"data": result}, status=status.HTTP_200_OK)

class ContentHandler(APIView):
    """ Set a new content (admin only)
    :params request.type: one of these values : HP, LLT, HLT, DF, DB, MB
    :params request.title: if type == HP, homepage's title
    :params request.subTitle: if type == HP, homepage's subtitle
    :params request.text: if type != HP, carousel's text
    :params request.video: if type != HP, carousel's video (optionnal)
    :params request.order: if type != HP, content's position on the carousel
    :returns: HTTP_Response 200 if ok, else 401 if user is not admin
    """
    my_tags = ["Content"]
    permission_classes = [IsAuthenticated]
    
    class ContentSwaggerSerializer(serializers.Serializer):
        type = serializers.CharField()
        title = serializers.CharField()
        subTitle = serializers.CharField()
        text = serializers.CharField()
        video = serializers.CharField()
        order = serializers.IntegerField()
        date = serializers.DateField()
        img = serializers.CharField()
        imgAlt = serializers.CharField()
        
    @swagger_auto_schema(
        operation_description="Set a new content (admin only)",
        request_body=ContentSwaggerSerializer,
        responses={200: "success", 401: "Unauthorized"}
    )
    def post(self, request: Request) -> JsonResponse:
        if request.user.is_superuser:
            content_type = request.data.get("type")
            if content_type == "HP":
                title = request.data.get("title")
                subTitle = request.data.get("subTitle")
                content = Content(title=title, subTitle=subTitle, type=content_type).save()
            elif content_type == "NEWS":
                title = request.data.get("title")
                date = request.data.get("date")
                text = request.data.get("text")
                img = request.data.get("img")
                img_alt = request.data.get("imgAlt")
                content = Content(title=title, date=date, text=text, img=img, img_alt=img_alt, type=content_type).save()
            elif content_type == "LN" or content_type == "RGPD":
                title = request.data.get("title")
                text = request.data.get("text")
                order = request.data.get("order")
                content = Content(title=title, text=text, order=order, type=content_type).save()
            else:
                text = request.data.get("text")
                video = request.data.get("video")
                order = request.data.get("order")
                content = Content(text=text, video=video, order=order, type=content_type).save()
            return JsonResponse({"success": "content created"}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({"forbidden": "you are not allowed"}, status=status.HTTP_401_UNAUTHORIZED)

    """ Update a content (admin only)
    :params request.type: one of these values : HP, LLT, HLT, DF, DB, MB
    :params request.title: if type == HP, homepage's title
    :params request.subTitle: if type == HP, homepage's subtitle
    :params request.text: if type != HP, carousel's text
    :params request.video: if type != HP, carousel's video (optionnal)
    :params request.order: if type != HP, content's position on the carousel
    :returns: HTTP_Response 200 if ok, else 401 if user is not admin
    """
    permission_classes = [IsAuthenticated]
    
    class ContentSerializer(serializers.Serializer):
        type = serializers.CharField()
        title = serializers.CharField()
        subTitle = serializers.CharField()
        text = serializers.CharField()
        video = serializers.CharField()
        order = serializers.IntegerField()
        date = serializers.DateField()
        img = serializers.CharField()
        imgAlt = serializers.CharField()
        
    @swagger_auto_schema(
        operation_description="Update a content (admin only)",
        request_body=ContentSerializer,
        responses={200: "success", 401: "Unauthorized"}
    )
    def put(self, request: Request, content_id: str) -> JsonResponse:
        if request.user.is_superuser:
            try:
                content = Content.objects.get(id=int(content_id))
                content_type = request.data.get("type")
                kwargs = {
                    "title": request.data.get("title"),
                    "subTitle": request.data.get("subTitle"),
                    "text": request.data.get("text"),
                    "video": request.data.get("video"),
                    "order": request.data.get("order"),
                    "date": request.data.get("date"),
                    "img": request.data.get("img"),
                    "img_alt": request.data.get("imgAlt")
                }
                content.update(**kwargs)
                return JsonResponse({"success": "content updated"}, status=status.HTTP_200_OK)
            except ObjectDoesNotExist:
                return JsonResponse({"error": "content not found"},status=status.HTTP_404_NOT_FOUND)
        else:
            return JsonResponse({"forbidden": "you are not allowed"}, status=status.HTTP_401_UNAUTHORIZED)

    """ Delete a content by its id (admin only)
    :params content_id: content's id to be deleted
    :returns: HTTP_Response 200 if ok, else 401 if user is not admin
    """
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        operation_description="Delete a content by its id (admin only)",
        responses={200: "success", 401: "Unauthorized"}
    )
    def delete(self, request: Request, content_id: str) -> JsonResponse:
        if request.user.is_superuser:
            try:
                content = Content.objects.get(id=content_id)
                content.delete()
                return JsonResponse({"success": "content deleted"}, status=status.HTTP_200_OK)
            except ObjectDoesNotExist:
                return JsonResponse({"error": "content not found"},status=status.HTTP_404_NOT_FOUND)
        else:
            return JsonResponse({"forbidden": "you are not allowed"}, status=status.HTTP_401_UNAUTHORIZED)