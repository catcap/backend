from django.apps import AppConfig


class ContentHandlerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'content_handler'
