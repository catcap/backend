import threading
from datetime import datetime

from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.contrib.sites.shortcuts import get_current_site

from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework import status

from authentication.models import User, Group
from authentication.authenticate import CustomJWTAuth
from Neo4j.models import Neo4j_Proxyma_Corpus
from WS_notifs.user_notify import user_notify

from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers



class HealthCheckView(APIView):
	""" Route for docker's healthcheck, will response True if the API is running
	:returns: boolean
	"""
	authentication_classes = []
	permission_classes = [AllowAny]
	@swagger_auto_schema(
    operation_description="Route for docker's healthcheck, will response True if the API is running",
    responses={200: "boolean"})
	def get(self, request: Request) -> Response:
		return Response(True)


class ContactView(APIView):
	""" Sends incoming message to all superusers through async thread
	:returns: HTTP_Response 204
	"""
	my_tags = ["Contact"]
	authentication_classes = []
	permission_classes = [AllowAny]
 
	class ContactSerializer(serializers.Serializer):
		from_ = serializers.CharField()
		subject = serializers.CharField()
		message = serializers.CharField()
  
	@swagger_auto_schema(
	operation_description="Sends incoming message to all superusers through async thread",
	request_body=ContactSerializer,
	responses={204: "No Content"})
	def post(self, request: Request) -> Response:
		try:
			sender = request.data["from"]
			subject = request.data["subject"]
			message = request.data["message"]
			super_users = User.objects.filter(is_superuser=True)
			thread = threading.Thread(target=self.send_contact_form_mail, args=(request, super_users, sender, subject, message))
			thread.start()
			return Response(status=status.HTTP_204_NO_CONTENT)
		except:
			return Response(status=status.HTTP_400_BAD_REQUEST)

	def send_contact_form_mail(self, request: Request, super_users: list[User], sender: str, subject: str, message: str) -> None:
		subject = f"Contact CaTCaP : {subject}"
		for user in super_users:
			message = render_to_string("contact_email.html", {
				"user": user,
				"sender": sender,
				"subject": subject,
				"message": message,
			})
			send_mail(subject, message, "admin@catcap.univ-smb.fr", [user.email], fail_silently=False)


class AdminUserView(APIView):
	""" Gets all users (admin only)
	:returns: HTTP_Response 200 with array of all users, 403 if user is not admin
	"""
	my_tags = ["Admin"]
	authentication_classes = [CustomJWTAuth]
	permission_classes = [AllowAny]
 
	class UserSerializer(serializers.Serializer):
		user_id = serializers.CharField()
		firstname = serializers.CharField()
		lastname = serializers.CharField()
		email = serializers.EmailField()
		type = serializers.CharField()
		is_active = serializers.BooleanField()
		last_login = serializers.DateTimeField()
		is_superuser = serializers.BooleanField()
		total_maps = serializers.IntegerField()

	@swagger_auto_schema(
	operation_description="Gets all users (admin only)",
	responses={200: UserSerializer(many=True)})
	def get(self, request: Request) -> Response:
		response = []
		for user in User.objects.all().order_by("last_name"):
			total_maps = 0
			for group in user.groups.all():
				total_maps += len(Neo4j_Proxyma_Corpus.nodes.filter(group=group.id))
			response.append({
				"user_id": user.id,
				"firstname": user.first_name,
				"lastname": user.last_name,
				"email": user.email,
				"type": user.get_type_display(),
				"is_active": user.is_active,
				"last_login": user.last_login,
				"is_superuser": user.is_superuser,
				"total_maps": total_maps,
			})
		return Response(response, status=status.HTTP_200_OK)
			
	""" Deletes selected user (admin only)
	:param user_id: string
	:returns: HTTP_Response 204 if success, 401 if user try to delete himself, 403 if user is not admin
	"""
	authentication_classes = [CustomJWTAuth]
	permission_classes = [AllowAny]
	@swagger_auto_schema(
	operation_description="Deletes selected user (admin only)",
	responses={204: "No Content", 401: "Unauthorized", 403: "Forbidden"})
	def delete(self, request: Request, user_id: str) -> Response:
		if user_id != request.user.id:
			user = User.objects.filter(id=user_id)
			user.delete()
			return Response(status=status.HTTP_204_NO_CONTENT)
		else:
			return Response({"message": "cannot_delete_yourself"}, status=status.HTTP_401_UNAUTHORIZED)

class AdminGroupView(APIView):
	""" Gets all groups (admin only)
	:returns: HTTP_Response 200 with array of groups, 403 if user is not admin
	"""
	my_tags = ["Admin"]
	authentication_classes = [CustomJWTAuth]
	permission_classes = [AllowAny]
	class GroupSerializer(serializers.Serializer):
		group_id = serializers.CharField()
		name = serializers.CharField()
		status = serializers.BooleanField()
		owner = serializers.CharField()
		total_members = serializers.IntegerField()
  
	@swagger_auto_schema(
	operation_description="Gets all groups (admin only)",
	responses={200: GroupSerializer(many=True)})
	def get(self, request: Request) -> Response:
		response = []
		for group in Group.objects.all().order_by("name"):
			response.append({
				"group_id": group.id,
				"name": group.name,
				"status": group.public_group,
				"owner": group.owner.get_full_name(),
				"total_members": group.get_total_members(),
			})
		return Response(response, status=status.HTTP_200_OK)

class AdminNotificationView(APIView):
	""" Calls send_servicing_email for all users (admin only)
	:returns: HTTP_Response 204, 403 if user is not admin
	"""
	my_tags = ["Admin"]
	authentication_classes = [CustomJWTAuth]
	permission_classes = [AllowAny]
	@swagger_auto_schema(
	operation_description="Calls send_servicing_email for all users (admin only)",
	responses={204: "No Content"})
 
	def post(self, request: Request) -> Response:
		try:
			startData = request.data["start"]
			endData = request.data["end"]
			message = f"SER_<{startData}>_<{endData}>"
			start = {
				"date": datetime.fromisoformat(startData).strftime("%d/%m/%Y"),
				"hour": datetime.fromisoformat(startData).strftime("%H:%M")
			}
			end = {
				"date": datetime.fromisoformat(endData).strftime("%d/%m/%Y"),
				"hour": datetime.fromisoformat(endData).strftime("%H:%M")
			}
			users = User.objects.filter(mail_agreement=True)
			thread = threading.Thread(target = self.send_servicing_email, args=(request, users, start, end))
			thread.start()
			return Response(status=status.HTTP_204_NO_CONTENT)
		except:
			return Response(status=status.HTTP_400_BAD_REQUEST)

	""" Sends email to one user to notify him of future maintenance (admin only)
	:params user: class User's instance
	:params start: class Datetime's instance, maintenance's begin
	:params end: class Datetime's instance, maintenance's end
	"""
	def send_servicing_email(self, request: Request, users: list[User], start: dict, end: dict) -> None:
		current_site = get_current_site(request)
		subject = "Maintenance CaTCaP"
		for user in users:
			message = render_to_string("servicing_email.html", {
				"user": user,
				"start": start,
				"end": end,
				"domain": current_site.domain
			})
			send_mail(subject, message, "admin@catcap.univ-smb.fr", [user.email], fail_silently=False)