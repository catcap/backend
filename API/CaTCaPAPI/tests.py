from authentication.models import User, Group
from django.test import TestCase
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist
from unittest.mock import patch
import json

class HealthCheckViewTest(TestCase):
	def test_get(self):
		response = self.client.get(reverse("healthcheck"))
		self.assertEqual(response.status_code, 200)
		self.assertEqual(bool(response.content.decode("utf-8")), True)

class ContactViewTest(TestCase):
	def test_post_200(self):
		with patch("CaTCaPAPI.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("contact"), {"from": "lePape", "subject": "pouet", "message": "pouet pouet"})
			self.assertEqual(response.status_code, 204)

	def test_post_400(self):
		with patch("CaTCaPAPI.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("contact"))
			self.assertEqual(response.status_code, 400)

class AdminUserViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True, is_superuser=True)
		self.jane_doe = User.objects.create_user(email="janedoe@mail.com", first_name="jane", last_name="doe", password="132456", is_active=True, is_superuser=False)

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		response = self.client.get(reverse("admin_users"))
		users = json.loads(response.content.decode("utf-8"))
		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(users), 2)

	def test_get_401(self):
		response = self.client.get(reverse("admin_users"))
		self.assertEqual(response.status_code, 401)
		response = self.client.post(reverse("login"), { "email": self.jane_doe.email, "password": "123456" })
		response = self.client.get(reverse("admin_users"))
		self.assertEqual(response.status_code, 401)

	def test_delete_200(self):
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		response = self.client.delete(reverse("admin_user_id", kwargs={"user_id": self.jane_doe.id}))
		with self.assertRaises(ObjectDoesNotExist):
			User.objects.get(email=self.jane_doe.email)
		self.assertEqual(response.status_code, 204)

	def test_delete_401(self):
		response = self.client.delete(reverse("admin_user_id", kwargs={"user_id": self.jon_doe.id}))
		self.assertEqual(response.status_code, 401)
		response = self.client.post(reverse("login"), { "email": self.jane_doe.email, "password": "123456" })
		response = self.client.delete(reverse("admin_user_id", kwargs={"user_id": self.jon_doe.id}))
		self.assertEqual(response.status_code, 401)
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		response = self.client.delete(reverse("admin_user_id", kwargs={"user_id": self.jon_doe.id}))
		self.assertEqual(response.status_code, 401)

class AdminGroupViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True, is_superuser=True)
		self.jane_doe = User.objects.create_user(email="janedoe@mail.com", first_name="jane", last_name="doe", password="132456", is_active=True, is_superuser=False)
		self.group1 = Group(name="group1", public_group=True, owner=self.jon_doe)
		self.group1.save()
		self.group2 = Group(name="group2", public_group=False, owner=self.jon_doe)
		self.group2.save()

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		response = self.client.get(reverse("admin_groups"))
		groups = json.loads(response.content.decode("utf-8"))
		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(groups), 2)

	def test_get_401(self):
		response = self.client.get(reverse("admin_groups"))
		self.assertEqual(response.status_code, 401)
		response = self.client.post(reverse("login"), { "email": self.jane_doe.email, "password": "123456" })
		response = self.client.get(reverse("admin_groups"))
		self.assertEqual(response.status_code, 401)

class AdminNotificationViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True, is_superuser=True)
		self.jane_doe = User.objects.create_user(email="janedoe@mail.com", first_name="jane", last_name="doe", password="132456", is_active=True, is_superuser=False)

	def test_post_204(self):
		with patch("CaTCaPAPI.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
			response = self.client.post(reverse("admin_notify"), {"start": "2024-04-30T12:00", "end": "2024-04-30T12:01"})
			self.assertEqual(response.status_code, 204)

	def test_post_400(self):
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		response = self.client.post(reverse("admin_notify"))
		self.assertEqual(response.status_code, 400)

	def test_post_401(self):
		response = self.client.post(reverse("admin_notify"), {"start": "2024-04-30T12:00", "end": "2024-04-30T12:01"})
		self.assertEqual(response.status_code, 401)
		response = self.client.post(reverse("login"), { "email": self.jane_doe.email, "password": "123456" })
		response = self.client.post(reverse("admin_notify"), {"start": "2024-04-30T12:00", "end": "2024-04-30T12:01"})
		self.assertEqual(response.status_code, 401)
