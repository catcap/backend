from django.contrib import admin
from django.urls import path, include
from CaTCaPAPI.views import *

from django.urls import re_path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Proxyma API",
      default_version='v1',
      description="Test description",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
   authentication_classes=[]
)

urlpatterns = [
    path('api/swagger<format>/', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('api/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    
    path('healthCheck', HealthCheckView.as_view(), name="healthcheck"),
    path('api/contact', ContactView.as_view(), name="contact"),
    path('api/admin/users', AdminUserView.as_view(), name="admin_users"),
    path('api/admin/users/<int:user_id>', AdminUserView.as_view(), name="admin_user_id"),
    path('api/admin/groups', AdminGroupView.as_view(), name="admin_groups"),
    path('api/admin/notification', AdminNotificationView.as_view(), name="admin_notify"),
    path('api/auth/', include('authentication.urls')),
    path('api/maps/', include('maps.urls')),
    path('api/data_handler/', include('data_handler.urls')),
    path('api/plug/', include('scraping_handler.urls')),
    path('api/content/', include('content_handler.urls'))
]
