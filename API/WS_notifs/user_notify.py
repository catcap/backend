from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.core.mail import send_mail

from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from authentication.models import Notification

""" Notifies a user via WebSocket, the channel name is defined by splitting the user's email on the @ character and keeping the first part, if notify failed, then sends email to the user
:params request: HTTP request, used to get the current site URL
:params user: class User's instance
:params message: a string that contains the message to send, can start with OK_ (map saved), GP_ (group created/joined/leaved/banned/deleted), SER_ (maintenance forecasting), DATA_ (scraped data ready)
:params subject: a string that contains the mail's subject (in case of notify failed)
:params template: a string that contains the mail's template name (html file)
:params corpus: a string that contains the corpus' name if a corpus is saved/modified, else None
:returns: None
"""
def user_notify(request, user, message, subject, template, corpus=None):
	channel_layer = get_channel_layer()
	notification = Notification(content=message if corpus is None else (message + corpus))
	notification.user = user
	notification.save()
	if channel_layer and corpus is None:
		async_to_sync(channel_layer.group_send)(
			user.email.split('@')[0],
			{
				"type": "send_message",
				"message": f"{notification.uid}_#_{notification.content}",
			}
		)
	elif channel_layer and corpus:
		async_to_sync(channel_layer.group_send)(
			user.email.split('@')[0],
			{
				"type": "send_message",
				"message": f"{notification.uid}_#_{notification.content}"
			}
		)
	else:
		current_site = get_current_site(request)
		subject = subject
		if corpus:
			message = render_to_string(template, {
				"user": user,
				"domain": current_site.domain,
				"corpus": corpus,
			})
		else:
			message = render_to_string(template, {
				"user": user,
				"domain": current_site.domain,
			})
		send_mail(subject, message, "simon.chauvel@univ-smb.fr", [user.email], fail_silently=False)