import json
from datetime import datetime, timezone
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from authentication.models import Notification, User

class NotificationConsumer(AsyncWebsocketConsumer):

	async def connect(self):
		self.email = self.scope['url_route']['kwargs']['email']
		self.user = await database_sync_to_async(self.get_user_by_email)(self.email)
		self.notifications = await database_sync_to_async(self.get_notifications_by_user)()
		self.group_name = self.email.split('@')[0]
		await self.accept()
		await self.channel_layer.group_add(self.group_name, self.channel_name)
		for notif in self.notifications:
			await self.channel_layer.group_send(
				self.group_name,
				{
					"type": notif["type"],
					"message": f"{notif['uid']}_#_{notif['message']}",
				}
			)

	async def disconnect(self, close_code):
		await self.channel_layer.group_discard(self.group_name, self.channel_name)

	async def receive(self, text_data):
		text_data_json = json.loads(text_data)
		notification_uid = text_data_json['notification_uid']
		await database_sync_to_async(self.delete_notification)(notification_uid)

	async def send_message(self, event):
		message = event['message']
		await self.send(text_data=json.dumps({'message': message}))


	def get_user_by_email(self, email):
		return User.objects.get(email=email)

	def get_notifications_by_user(self):
		user_id = self.user.id
		notifications = Notification.objects.filter(user=user_id)
		notifs = []
		for notif in notifications:
			now = datetime.now(timezone.utc)
			diff = abs(now-notif.created_at).days
			if diff <= 3:
				notifs.append({
						"type": "send_message",
						"message": notif.content,
						"uid": notif.uid
				})
			else:
				notif.delete()
		return notifs

	def delete_notification(self, notification_uid):
		notification = Notification.objects.get(uid=notification_uid)
		notification.delete()
