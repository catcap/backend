from django.urls import path
from authentication.views import *

urlpatterns = [
    # JWT views
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name="logout"),
    path('verify', TokenVerifyView.as_view(), name="verify"),
    path('refresh', RefreshTokenView.as_view(), name="refresh"),

    # Register views
    path('register', RegisterView.as_view(), name="register"),
    path('confirm_register/<str:uidb64>/<str:token>', ConfirmRegistrationView.as_view(), name="confirm_register"),

    # Password views
    path('forgotten_password', ForgottenPasswordView.as_view(), name="forgotten_password"),
    path('reset_password/<str:uidb64>/<str:token>', ResetPasswordView.as_view(), name="reset_password"),
    path('change_password', ChangePasswordView.as_view(), name="change_password"),

    # Delete account views
    path('delete_account', DeleteAccountView.as_view(), name="delete_account"),
    path('confirm_delete_account/<str:uidb64>/<str:token>', ConfirmationDeleteAccountView.as_view(), name="confirm_delete_account"),

    # Me route
    path('me', Me.as_view(), name="me"),

    # Groups/SubGroups routes
    path('group', Groups.as_view(), name="group"),
    path('group/<str:group_id>', Groups.as_view(), name="group_id"),
]
