from authentication.models import User, Group, Notification
from django.test import TestCase
from django.urls import reverse
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist
from unittest.mock import patch
import datetime, json

class UserTestCase(TestCase):
	def setUp(self):
		User.objects.create_user("jondoe@email.com", "123456", first_name="Jon", last_name="Doe", type="STUDENT")
		self.jon_doe = User.objects.get(email="jondoe@email.com")

	def test_user_exists(self):
		self.assertEqual(self.jon_doe.first_name, "Jon")
		self.assertEqual(self.jon_doe.last_name, "Doe")

	def test_user_activate(self):
		self.assertEqual(self.jon_doe.is_active, False)
		self.jon_doe.is_active = True
		self.jon_doe.save()
		self.assertEqual(self.jon_doe.is_active, True)
		
	def test_user_is_superuser(self):
		self.assertEqual(self.jon_doe.is_superuser, False)
		self.jon_doe.is_superuser = True
		self.jon_doe.save()
		self.assertEqual(self.jon_doe.is_superuser, True)

	def test_user_mail_agreement(self):
		self.assertEqual(self.jon_doe.mail_agreement, True)
		self.jon_doe.mail_agreement = False
		self.jon_doe.save()
		self.assertEqual(self.jon_doe.mail_agreement, False)

	def test_user_get_mail(self):
		self.assertEqual(self.jon_doe.__str__(), "jondoe@email.com")

	def test_user_get_full_name(self):
		self.assertEqual(self.jon_doe.get_full_name(), "Jon Doe")

	def test_user_get_owned_groups(self):
		self.assertEqual(len(self.jon_doe.get_owned_groups()), 0)

	def test_user_get_type_display(self):
		self.assertEqual(self.jon_doe.get_type_display(), "Étudiant⋅e")
	
	def test_user_update(self):
		self.jon_doe.update(first_name="Jane", type="OTHER", mail_agreement=False)
		self.assertEqual(self.jon_doe.first_name, "Jane")
		self.assertEqual(self.jon_doe.type, "OTHER")
		self.assertEqual(self.jon_doe.mail_agreement, False)

class GroupTestCase(TestCase):
	def setUp(self):
		User.objects.create_user("jondoe@email.com", "123456", first_name="Jon", last_name="Doe", type="STUDENT")
		self.jon_doe = User.objects.get(email="jondoe@email.com")
		User.objects.create_user("janedoe@email.com","123456", first_name="Jane", last_name="Doe")
		self.jane_doe = User.objects.get(email="janedoe@email.com")
		Group.objects.create(owner=self.jon_doe).save()
		self.group = Group.objects.get(name="Groupe privé")
		self.jon_doe.last_active_group = self.group
		
	
	def test_group_exists(self):
		self.assertEqual(self.group.name, "Groupe privé")
		self.assertEqual(self.group.public_group, False)
		self.assertEqual(self.group.owner, self.jon_doe)

	def test_group_str(self):
		self.assertEqual(self.group.__str__(), "Groupe privé")

	def test_group_verify_owner(self):
		self.assertEqual(self.group.verify_owner(self.jon_doe), True)
		self.assertEqual(self.group.verify_owner(self.jane_doe), False)

	def test_group_is_active(self):
		self.assertEqual(self.group.is_active(self.jon_doe), True)
		self.assertEqual(self.group.is_active(self.jane_doe), False)

	def test_group_update_name(self):
		self.assertEqual(self.group.name, "Groupe privé")
		self.group.update(name="Groupe public")
		self.assertEqual(self.group.name, "Groupe public")

	def test_group_update_status(self):
		self.assertEqual(self.group.public_group, False)
		self.group.update(public_group=True)
		self.assertEqual(self.group.public_group, True)

	def test_group_update_members(self):
		self.group.update(add_user=self.jane_doe, remove_user=None)
		self.assertIn(self.group, self.jane_doe.groups.all())
		self.group.update(add_user=None, remove_user=self.jane_doe)
		self.assertNotIn(self.group, self.jane_doe.groups.all())

class NotificationTestCase(TestCase):
	def setUp(self):
		User.objects.create_user("jondoe@email.com", "123456", first_name="Jon", last_name="Doe", type="STUDENT")
		self.jon_doe = User.objects.get(email="jondoe@email.com")
		User.objects.create_user("janedoe@email.com","123456", first_name="Jane", last_name="Doe")
		self.jane_doe = User.objects.get(email="janedoe@email.com")
		Notification.objects.create(content="test", user=self.jon_doe).save()
		self.notification = Notification.objects.get(content="test")

	def test_notification_exists(self):
		self.assertEqual(self.notification.content, "test")
		self.assertEqual(type(self.notification.created_at), datetime.datetime)
		self.assertEqual(self.notification.user, self.jon_doe)
		self.assertNotEqual(self.notification.user, self.jane_doe)

class LoginViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		self.jane_doe = User.objects.create_user(email="janedoe@mail.com", first_name="jane", last_name="doe", password="123456", is_active=False)

	def test_post_200(self):
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		access_token = response.cookies["access_token"]
		refresh_token = response.cookies["refresh_token"]
		self.assertEqual(response.status_code, 200)
		self.assertIsNotNone(access_token)
		self.assertIsNotNone(refresh_token)

	def test_post_401(self):
		response = self.client.post(reverse("login"), {"email": self.jane_doe.email, "password": "123456"})
		self.assertEqual(response.status_code, 401)
		respone = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "12345"})
		self.assertEqual(response.status_code, 401)
		respone = self.client.post(reverse("login"), {"email": "jonedoe@mail.com", "password": "123456"})
		self.assertEqual(response.status_code, 401)

class TokenVerifyViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		response = self.client.get(reverse("verify"))
		self.assertEqual(response.status_code, 200)

	def test_get_401(self):
		response = self.client.get(reverse("verify"))
		self.assertEqual(response.status_code, 401)

class RefreshTokenViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		access_token = response.cookies["access_token"]
		refresh_token = response.cookies["refresh_token"]
		response = self.client.get(reverse("refresh"))
		new_access_token = response.cookies["access_token"]
		new_refresh_token = response.cookies["refresh_token"]
		self.assertEqual(response.status_code, 200)
		self.assertNotEqual(access_token, new_access_token)
		self.assertNotEqual(refresh_token, new_refresh_token)

	def test_get_401(self):
		response = self.client.get(reverse("refresh"))
		self.assertEqual(response.status_code, 401)

class LogoutViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "123456" })
		response = self.client.get(reverse("logout"))
		self.assertEqual(response.status_code, 200)
		response = self.client.get(reverse("refresh"))
		self.assertEqual(response.status_code, 401)

	def test_get_401(self):
		response = self.client.get(reverse("logout"))
		self.assertEqual(response.status_code, 401)

class RegisterViewTest(TestCase):
	def test_post_200(self):
		with patch("authentication.views.RegisterView.send_confirmation_email", return_value=None):
			response = self.client.post(reverse("register"), {"firstname": "jon", "lastname": "doe", "password": "123456", "email": "jondoe@mail.com", "type": "NR"})
			jon_doe = User.objects.get(email="jondoe@mail.com")
			self.assertEqual(response.status_code, 200)
			self.assertIsNotNone(jon_doe)
			self.assertIsNotNone(jon_doe.last_active_group)
	
	def test_post_500(self):
		with patch("authentication.views.RegisterView.send_confirmation_email", return_value=None):
			response = self.client.post(reverse("register"), {})
			self.assertEqual(response.status_code, 500)

class ConfirmRegistrationViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=False)

	def test_get_200(self):
		uid = urlsafe_base64_encode(force_bytes(self.jon_doe.pk))
		token = default_token_generator.make_token(self.jon_doe)
		response = self.client.get(reverse("confirm_register", kwargs={"uidb64": uid, "token": token}))
		self.jon_doe = User.objects.get(email="jondoe@mail.com")
		self.assertEqual(response.status_code, 200)
		self.assertEqual(self.jon_doe.is_active, True)

	def test_get_400(self):
		response = self.client.get(reverse("confirm_register", kwargs={"uidb64": "falseUID", "token": "falseToken"}))
		self.assertEqual(response.status_code, 400)

class ForgottenPasswordViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_post_200(self):
		with patch("authentication.views.ForgottenPasswordView.send_reset_password_email", return_value=None):
			response = self.client.post(reverse("forgotten_password"), {"email": self.jon_doe.email})
			self.assertEqual(response.status_code, 200)

	def test_post_404(self):
		with patch("authentication.views.ForgottenPasswordView.send_reset_password_email", return_value=None):
			response = self.client.post(reverse("forgotten_password"), {"email": "jonedoe@mail.com"})
			self.assertEqual(response.status_code, 404)

class ResetPasswordViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_post_200(self):
		uid = urlsafe_base64_encode(force_bytes(self.jon_doe.pk))
		token = default_token_generator.make_token(self.jon_doe)
		response = self.client.post(reverse("reset_password", kwargs={"uidb64": uid, "token": token}), {"password": "654321"})
		self.assertEqual(response.status_code, 200)
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "654321" })
		self.assertEqual(response.status_code, 200)

	def test_post_400(self):
		response = self.client.post(reverse("reset_password", kwargs={"uidb64": "falseUID", "token": "falseToken"}), {"password": "654321"})
		self.assertEqual(response.status_code, 400)
		uid = urlsafe_base64_encode(force_bytes(self.jon_doe.pk))
		token = default_token_generator.make_token(self.jon_doe)
		response = self.client.post(reverse("reset_password", kwargs={"uidb64": uid, "token": token}), {})
		self.assertEqual(response.status_code, 400)

class ChangePasswordViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_post_200(self):
		response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
		response = self.client.post(reverse("change_password"), {"old_password": "123456", "password": "654321"})
		self.assertEqual(response.status_code, 200)
		response = self.client.post(reverse("login"), { "email": self.jon_doe.email, "password": "654321" })
		self.assertEqual(response.status_code, 200)

	def test_post_401(self):
		response = self.client.post(reverse("change_password"), {"old_password": "123456", "password": "654321"})
		self.assertEqual(response.status_code, 401)
		response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
		response = self.client.post(reverse("change_password"), {"old_password": "1234", "password": "654321"})
		self.assertEqual(response.status_code, 401)

class DeleteAccountViewTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_post_200(self):
		with patch("authentication.views.DeleteAccountView.send_account_deletion_validation_email", return_value=None):
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			response = self.client.post(reverse("delete_account"), {"password": "123456"})
			self.assertEqual(response.status_code, 200)

	def test_post_401(self):
		with patch("authentication.views.DeleteAccountView.send_account_deletion_validation_email", return_value=None):
			response = self.client.post(reverse("delete_account"), {"password": "132456"})
			self.assertEqual(response.status_code, 401)
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			response = self.client.post(reverse("delete_account"), {"password": "1324"})
			self.assertEqual(response.status_code, 401)

class ConfirmationDeleteAccountView(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_delete_200(self):
		with patch("authentication.views.ConfirmationDeleteAccountView.send_account_deleted_confirmation_email", return_value=None):
			uid = urlsafe_base64_encode(force_bytes(self.jon_doe.pk))
			token = default_token_generator.make_token(self.jon_doe)
			response = self.client.delete(reverse("confirm_delete_account", kwargs={"uidb64": uid, "token": token}))
			self.assertEqual(response.status_code, 200)

	def test_delete_400(self):
		with patch("authentication.views.ConfirmationDeleteAccountView.send_account_deleted_confirmation_email", return_value=None):
			response = self.client.delete(reverse("confirm_delete_account", kwargs={"uidb64": "fakeUID", "token": "fakeToken"}))
			self.assertEqual(response.status_code, 400)

class MeTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_get_200(self):
		response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
		response = self.client.post(reverse("me"))
		self.assertEqual(response.status_code, 200)

	def test_get_401(self):
		response = self.client.post(reverse("me"))
		self.assertEqual(response.status_code, 401)

	def test_post_200(self):
		response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
		response = self.client.post(reverse("me"), {"firstname": "jane"})
		self.jon_doe = User.objects.get(first_name="jane")
		self.assertEqual(response.status_code, 200)
		self.assertEqual(self.jon_doe.first_name, "jane")

	def test_post_401(self):
		response = self.client.post(reverse("me"), {"firstname": "jane"})
		self.assertEqual(response.status_code, 401)
		with self.assertRaises(ObjectDoesNotExist):
			self.jon_doe = User.objects.get(first_name="jane")

class GroupsTest(TestCase):
	def setUp(self):
		self.jon_doe = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		self.public_group = Group.objects.create(name="public", public_group=True, owner=self.jon_doe)
		self.private_group = Group.objects.create(name="private", public_group=False, owner=self.jon_doe)
		self.jon_doe.groups.add(self.public_group.id)
		self.jon_doe.groups.add(self.private_group.id)
	
	def test_get_200(self):
		response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
		response = self.client.get(reverse("group"))
		groups = response.json()["groups"]
		public_groups = response.json()["public_groups"]
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(groups, list)
		self.assertIsInstance(public_groups, list)
		self.assertEqual(len(groups), 2)
		self.assertEqual(len(public_groups), 0)

	def test_get_401(self):
		response = self.client.get(reverse("group"))
		self.assertEqual(response.status_code, 401)
	
	def test_post_200(self):
		with patch("WS_notifs.user_notify.user_notify", return_value=None):
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			response = self.client.post(reverse("group"), {"group_name": "private2", "group_users": [None], "group_status": False})
			group = Group.objects.get(name="private2")
			self.assertEqual(response.status_code, 201)
			self.assertEqual(group.public_group, False)

	def test_post_401(self):
		with patch("WS_notifs.user_notify.user_notify", return_value=None):
			response = self.client.post(reverse("group"), {"group_name": "private2", "group_users": [None], "group_status": False})
			self.assertEqual(response.status_code, 401)
			with self.assertRaises(ObjectDoesNotExist):
				group = Group.objects.get(name="private2")

	def test_put_200(self):
		with patch("WS_notifs.user_notify.user_notify", return_value=None):
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			response = self.client.put(reverse("group_id", kwargs={"group_id": self.private_group.id}), {"group_status": True}, content_type="application/json")
			self.assertEqual(response.status_code, 200)

	def test_put_401(self):
		with patch("WS_notifs.user_notify.user_notify", return_value=None):
			response = self.client.put(reverse("group_id", kwargs={"group_id": self.private_group.id}), {"group_status": True}, content_type="application/json")
			self.assertEqual(response.status_code, 401)
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			response = self.client.put(reverse("group_id", kwargs={"group_id": self.private_group.id}), {"group_member": self.jon_doe.email}, content_type="application/json")
			self.assertEqual(response.status_code, 401)

	def test_put_404(self):
		with patch("WS_notifs.user_notify.user_notify", return_value=None):
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			response = self.client.put(reverse("group_id", kwargs={"group_id": self.private_group.id}), {"group_member": "janedoe@mail.com"}, content_type="application/json")
			self.assertEqual(response.status_code, 404)

	def test_put_406(self):
		with patch("WS_notifs.user_notify.user_notify", return_value=None):
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			jane_doe = User.objects.create_user(email="janedoe@mail.com", password="123456", first_name="jane", last_name="doe")
			jane_doe.groups.add(self.private_group.id)
			response = self.client.put(reverse("group_id", kwargs={"group_id": self.private_group.id}), {"group_member": jane_doe.email}, content_type="application/json")
			self.assertEqual(response.status_code, 406)

	def test_delete_204(self):
		with patch("WS_notifs.user_notify.user_notify", return_value=None):
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			response = self.client.delete(reverse("group_id", kwargs={"group_id": self.private_group.id}))
			self.assertEqual(response.status_code, 204)
			with self.assertRaises(ObjectDoesNotExist):
				Group.objects.get(name="private")

	def test_delete_401(self):
		with patch("WS_notifs.user_notify.user_notify", return_value=None):
			response = self.client.delete(reverse("group_id", kwargs={"group_id": self.private_group.id}))
			self.assertEqual(response.status_code, 401)
			self.public_group.owner = None
			self.jon_doe.groups.remove(self.public_group.id)
			response = self.client.post(reverse("login"), {"email": self.jon_doe.email, "password": "123456"})
			response = self.client.delete(reverse("group_id", kwargs={"group_id": self.private_group.id}))
			self.assertEqual(response.status_code, 401)
			response = self.client.delete(reverse("group_id", kwargs={"group_id": self.public_group.id}))
			self.assertEqual(response.status_code, 401)