from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
import uuid

from authentication.models.UserManager import UserManager

class User(AbstractBaseUser, PermissionsMixin):
	TYPE_CHOICES = [
		("NR", "Non renseigné"),
		("STUDENT", "Étudiant⋅e"),
		("TEACHER", "Enseignant⋅e"),
		("RESEARCHER", "Chercheur⋅e"),
		("LECTURER-RESEARCHER", "Enseignant⋅e-chercheur⋅e"),
		("COMPANY", "Professionnel entreprise"),
		("OTHER", "Professionnel autre"),
	]

	first_name = models.CharField(max_length=150, blank=True)
	last_name = models.CharField(max_length=150, blank=True)
	email = models.EmailField(blank=True, unique=True)
	type = models.CharField(max_length=20, choices=TYPE_CHOICES, default="NR")
	is_active = models.BooleanField(default=False)
	password = models.CharField()
	mail_agreement = models.BooleanField(default=True)
	last_active_group = models.ForeignKey('Group', on_delete=models.DO_NOTHING, related_name='users_active_group', blank=True, null=True)
	groups = models.ManyToManyField('Group', related_name='users', blank=True)

	USERNAME_FIELD = "email"
	REQUIRED_FIELDS = [first_name, last_name, email, password]

	objects = UserManager()

	def __str__(self):
		return self.email

	def get_full_name(self):
		full_name = "%s %s" % (self.first_name, self.last_name)
		return full_name.strip()

	def get_owned_groups(self):
		groups = self.groups.all()
		for group in groups:
			if not group.verify_owner(self):
				groups.remove(group)
		return groups

	def get_type_display(self):
		return dict(self.TYPE_CHOICES).get(self.type, "Non renseigné")

	def update(self, **kwargs):
		for attr_name in vars(self):
			if attr_name in kwargs and kwargs[attr_name] is not None:
				setattr(self, attr_name, kwargs[attr_name])
				self.save()