import uuid
from django.db import models
from authentication.models import User

class Notification(models.Model):
	uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
	content = models.CharField(max_length=255)
	created_at = models.DateTimeField(auto_now_add=True)

	# Relations
	user = models.ForeignKey("User", on_delete=models.CASCADE, related_name="notifications", null=False)