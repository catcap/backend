from authentication.models.User import User
from django.db import models
from Neo4j.models import *
from django.utils import timezone
import datetime

class Group(models.Model):
	id = models.AutoField(primary_key=True, editable=False)
	name = models.CharField(max_length=150, default="Groupe privé")
	public_group = models.BooleanField(default=False)
	owner = models.ForeignKey("User", on_delete=models.CASCADE, related_name="groups_owner", null=True, default=None)

	def __str__(self) -> str:
		return self.name
		
	def get_total_members(self) -> int:
		members = self.users.all()
		return len(members)

	def verify_owner(self, user: User) -> bool:
		return self.owner == user

	def verify_member(self, user: User) -> bool:
		return user in self.users.all()
		
	def is_active(self, user: User) -> bool:
		return self == user.last_active_group

	def update(self, **kwargs: User) -> None:
		for attr_name in vars(self):
			if "add_user" in kwargs or "remove_user" in kwargs:
				if self.verify_owner(kwargs["add_user"]) or self.verify_owner(kwargs["remove_user"]):
					return
				elif kwargs["remove_user"] is not None:
					if self.verify_member(kwargs["remove_user"]) and not self.verify_owner(kwargs["remove_user"]):
						user = User.objects.get(email=kwargs["remove_user"])
						user.groups.remove(self)
						user.save()
				elif kwargs["add_user"] is not None:
					if  not self.verify_member(kwargs["add_user"]) and not self.verify_owner(kwargs["add_user"]):
						user = User.objects.get(email=kwargs["add_user"])
						user.groups.add(self)
						user.save()					
			elif attr_name in kwargs and kwargs[attr_name] is not None:
				setattr(self, attr_name, kwargs[attr_name])
				self.save()

	def create_example_corpus(self):
		proxyma = Neo4j_Proxyma_Corpus(title="Carto_exemple", group=self.id).save()
		actions = Neo4j_Action.get_or_create(
			{
				"uid": "aa0622e0-9680-4b20-a2ef-02db14088657",
      			"description": "Action 01"
			},
			{
				"uid": "7ef09b40-0081-4349-95b2-39d78c78bb52",
				"description": "Action 02"
			},
			{
				"uid": "db99653d-eeff-4549-a478-2a3e94ec86b5",
				"description": "Action 03"
			},
			{
				"uid": "9a33e454-91c2-4e70-b91d-1d262b5f8e20",
				"description": "Action 04"
   			}
		)
		activity = Neo4j_Activity.get_or_create(
			{
				"uid": "0b429301-e19d-449b-9d52-9c57383a296a",
				"description": "Activité",
    		}
		)
		actors = Neo4j_Actor.get_or_create(
			{
				"uid": "5c5f7d6e-91e1-4ec3-bdee-3be45813470f",
				"name": "Acteur 02",
    		},
			{
				"uid": "289d220d-7c93-4dcf-a4d3-2fc73faf6556",
				"name": "Acteur 01",
    		},
		)
		environmental_resources = Neo4j_Environmental_Resource.get_or_create(
			{
				"uid": "35288e57-0d77-44df-9aca-e5e6c7184a91",
      			"type": "env",
      			"description": "Ressource environmentale 01",
			},
			{
				"uid": "ca8c6b0e-c03e-4715-8745-3f793f9d382c",
      			"type": "env",
      			"description": "Ressource environmentale 02",
			}
		)
		objective = Neo4j_Objective.get_or_create(
			{
				"uid": "f226457f-5c7e-42f6-83b8-d0a0e1977a0e",
      			"description": "Objectif",
			}
		)
		performances = Neo4j_Performance.get_or_create(
			{
				"uid": "36eb5ed1-c2ef-4702-9f49-a9a1ef1d9164",
      			"criterion": "Performance 01",
      			"value": "80",
      			"unit": "mm",
			},
			{
				"uid": "cd0b20e8-c41c-44d2-ad16-71262df67371",
				"criterion": "Performance 02",
				"value": "75",
				"unit": "%",
			}
		)
		personnal_resources = Neo4j_Personnal_Resource.get_or_create(
			{
				"uid": "a3752432-899a-49d5-97b4-07f8cadfd0be",
				"type": "k",
				"description": "Ressource personnelle 01",
			},
			{
				"uid": "a18ba047-8399-4273-8192-9c73045fdfe3",
      			"type": "s",
      			"description": "Ressource personnelle 02",
			}
		)
		schemes = Neo4j_Scheme.get_or_create(
			{
				"uid": "805995c9-1155-4384-839a-f543aade3968",
      			"actions_sequence": "Scheme 01",
			},
			{
				"uid": "e742a3af-2e8b-4828-b967-d6b7c2796c63",
      			"actions_sequence": "Scheme 02",
			}
		)
		situation = Neo4j_Situation.get_or_create(
			{
				"uid": "c362733f-9c4c-45ee-b613-357d411068a4",
      			"description": "Situation",
			}
		)
		for index, action in enumerate(actions):
			proxyma.action.connect(action)
			schemes[0 if index%2==0 else 1].action.connect(action)


		proxyma.activity.connect(activity[0])


		for index, actor in enumerate(actors):
			proxyma.actor.connect(actor)
			schemes[index].actor.connect(actor)


		for index, environmental_resource in enumerate(environmental_resources):
			proxyma.environmental_resource.connect(environmental_resource)
			schemes[index].environmental_resource.connect(environmental_resource)


		proxyma.objective.connect(objective[0])
		activity[0].objective.connect(objective[0])


		for performance in performances:
			proxyma.performance.connect(performance)
			objective[0].performance.connect(performance)


		for index, personnal_resource in enumerate(personnal_resources):
			proxyma.personnal_resource.connect(personnal_resource)
			actors[index].personnal_resource.connect(personnal_resource)

		for scheme in schemes:
			proxyma.scheme.connect(scheme)
			activity[0].scheme.connect(scheme)
		proxyma.situation.connect(situation[0])
		activity[0].situation.connect(situation[0])
		
		proxyma.save()
		activity[0].save()
		for actor in actors:
			actor.save()
		for scheme in schemes:
			scheme.save()

		proxyma = Neo4j_Proxyma_Corpus(title="Top_Chef", group=self.id).save()
		actions = Neo4j_Action.get_or_create(
			{
				"uid": "9bef3327-3912-4a9b-ba4c-00390290856d",
				"description": "Dorer"
			},
			{
				"uid": "ac1e568c-c00b-44a7-982b-7a55261c677d",
				"description": "Mixer"
    		},
			{
				"uid": "cae75939-ca86-46e1-9a7b-e1a31eca9415",
				"description": "Éplucher"
			},
			{
				"uid": "74d02ceb-0b6c-472c-9a62-6e2eec5c15cd",
				"description": "Couper"
			},
			{
				"uid": "9be731df-9940-4ec2-90b6-fa860cdf0ffe",
      			"description": "Cuire sous pression"
			}
		)
		activity = Neo4j_Activity.get_or_create(
			{
				"uid": "8c1f466c-dba3-4ba8-9539-18477810e211",
      			"description": "Cuisiner"
			},

		)
		actor = Neo4j_Actor.get_or_create(
			{
				"uid": "1c826545-7bd2-4a01-b6a2-5473168223c2",
     			"name": "Cuisinier⋅e"
			}
		)
		environmental_resources = Neo4j_Environmental_Resource.get_or_create(
			{
				"uid": "bc9d68f9-8dfa-4620-8d6f-a0d2a7238616",
				"description": "Livre de recettes",
				"type": "livre"
			},
			{
				"uid": "cb0d6ead-97e5-42d1-8a7e-594862aaf2f0",
      			"description": "Légumes",
      			"type": "ingrédients"
			},
			{
				"uid": "10a49207-acfd-40fc-b548-d3e76f65a660",
      			"description": "Condiments",
      			"type": "ingrédients"
			}
		)
		objective = Neo4j_Objective.get_or_create(
			{
				"uid": "bf107e2f-2a0e-4d1a-a43f-138d8e35aaf0",
      			"description": "Faire une soupe"
			}
		)
		performance = Neo4j_Performance.get_or_create(
			{
				"uid": "8096612e-8745-4210-9e1a-ef6bdce422f2",
      			"criterion": "Temps de préparation",
      			"value": "20",
      			"unit": "minutes"
			},
			{
				"uid": "0db3a86f-2fe9-4903-8d0c-953fb9544b47",
				"criterion": "Temps de cuisson",
				"value": "30",
				"unit": "minutes"
			}
		)
		personnal_resources = Neo4j_Personnal_Resource.get_or_create(
			{
				"uid": "67eaf146-be87-40e4-bce2-2b61e5ecf544",
      			"description": "Cours de cuisine",
      			"type": "k"
			},
			{
				"uid": "d1d7c8ef-e770-4e9c-9589-b58986087f82",
      			"description": "Caractéristiques des ingrédients",
      			"type": "k"
			},
			{
				"uid": "219344bd-d6c0-4c1e-bfb7-a7843219aa99",
      			"description": "Techniques de cuisson",
      			"type": "s"
			},
			{
				"uid": "ecf9c838-0624-4f66-9a11-2769d9b48aaa",
      			"description": "Créativité",
      			"type": "a"
			}
		)
		schemes = Neo4j_Scheme.get_or_create(
			{
				"uid": "3177db6a-7e33-4b03-8b59-4fa11c0c33b5",
      			"actions_sequence": "Préparation"
			},
			{
				"uid": "11da2f22-4740-4005-bab4-a4e2e33e07b8",
      			"actions_sequence": "Cuisson"
			}
		)
		situation = Neo4j_Situation.get_or_create(
			{
				"uid": "d6eac932-d209-4dc1-bf31-3d61daa7f49c",
      			"description": "Cantine scolaire"
			}
		)
		for action in actions:
			proxyma.action.connect(action)
			if action.uid in ["cae75939-ca86-46e1-9a7b-e1a31eca9415", "74d02ceb-0b6c-472c-9a62-6e2eec5c15cd"]:
				schemes[0].action.connect(action)
			else:
				schemes[1].action.connect(action)

		proxyma.activity.connect(activity[0])

		proxyma.actor.connect(actor[0])			

		for environmental_resource in environmental_resources:
			proxyma.environmental_resource.connect(environmental_resource)
			if environmental_resource.uid in ["cb0d6ead-97e5-42d1-8a7e-594862aaf2f0", "10a49207-acfd-40fc-b548-d3e76f65a660", "bc9d68f9-8dfa-4620-8d6f-a0d2a7238616"]:
				schemes[0].environmental_resource.connect(environmental_resource)
			else:
				schemes[1].environmental_resource.connect(environmental_resource)
		
		proxyma.objective.connect(objective[0])
		activity[0].objective.connect(objective[0])

		for performance in performances:
			proxyma.performance.connect(performance)
			objective[0].performance.connect(performance)

		for personnal_resource in personnal_resources:
			proxyma.personnal_resource.connect(personnal_resource)
			actor[0].personnal_resource.connect(personnal_resource)
		
		for scheme in schemes:
			scheme.actor.connect(actor[0])
			proxyma.scheme.connect(scheme)
			activity[0].scheme.connect(scheme)

		proxyma.situation.connect(situation[0])
		activity[0].situation.connect(situation[0])

		proxyma.save()
		activity[0].save()
		actor[0].save()
		for scheme in schemes:
			scheme.save()