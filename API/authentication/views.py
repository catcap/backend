from django.conf import settings
from django.middleware import csrf
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.contrib.auth import update_session_auth_hash, authenticate
from django.core.mail import send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_str
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken

import re, datetime

from authentication.models import User, Group
from authentication.authenticate import CustomJWTAuth
from WS_notifs.user_notify import user_notify

from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers

"""Generates a JWT refresh_token and a JWT access_token from the refresh_token
:params user: class User's instance
:returns: dict with refresh_token and access_token
"""
def get_tokens_for_user(user: User) -> dict:
    refresh = RefreshToken.for_user(user)
    return {
        "refresh": str(refresh),
        "access": str(refresh.access_token),
    }


class LoginView(APIView):
    """ Connects one user to API
    :params request.email: user's email
    :params request.password: user's password
    :returns: HTTP_Response 200 with JWT access_token and JWT refresh_token in cookies, 401 if wrong credentials
    """
    my_tags = ["Authentification"]
    authentication_classes = []
    permission_classes = [AllowAny]
    class LoginSerializer(serializers.Serializer):
        email = serializers.EmailField()
        password = serializers.CharField()
    @swagger_auto_schema(
        operation_description="Connects one user to API",
        request_body=LoginSerializer,
        responses={200: "JWT access_token and JWT refresh_token in cookies", 401: "wrong credentials"})
    def post(self, request: Request) ->Response:
        email = request.data.get('email')
        password = request.data.get('password')
        user = authenticate(email=email, password=password)
        if user is not None:
            if user.is_active:
                response = Response()
                data = get_tokens_for_user(user)
                response.set_cookie(
                    key = settings.SIMPLE_JWT["AUTH_COOKIE"],
                    value = data["access"],
                    domain = settings.SIMPLE_JWT["AUTH_COOKIE_DOMAIN"],
                    expires = settings.SIMPLE_JWT["ACCESS_TOKEN_LIFETIME"],
                    secure = settings.SIMPLE_JWT["AUTH_COOKIE_SECURE"],
                    httponly = settings.SIMPLE_JWT["AUTH_COOKIE_HTTP_ONLY"],
                    samesite = settings.SIMPLE_JWT["AUTH_COOKIE_SAMESITE"]
                )
                response.set_cookie(
                    key = settings.SIMPLE_JWT["REFRESH_COOKIE"],
                    value = data["refresh"],
                    domain = settings.SIMPLE_JWT["AUTH_COOKIE_DOMAIN"],
                    expires = settings.SIMPLE_JWT["REFRESH_TOKEN_LIFETIME"],
                    secure = settings.SIMPLE_JWT["AUTH_COOKIE_SECURE"],
                    httponly = settings.SIMPLE_JWT["AUTH_COOKIE_HTTP_ONLY"],
                    samesite = settings.SIMPLE_JWT["AUTH_COOKIE_SAMESITE"]
                )
                csrf.get_token(request)
                response.data = {"success": "logged_in"}
                response.status_code = 200
                user.last_login = timezone.make_aware(datetime.datetime.now())
                user.save()
                return response
            else:
                return Response({"error": "user_inactive"})
        else:
            return Response({"error": "invalid_email_or_password"}, status=status.HTTP_401_UNAUTHORIZED)

class TokenVerifyView(APIView):
    """ Verifies if the JWT access_token is still valid through the authentication_class CustomJWTAuth
    :params request.access_token: JWT access_token
    :returns: HTTP_Response 200 if JWT access_token is ok
    """
    my_tags = ["Token"]
    authentication_classes = [CustomJWTAuth]
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_description="Verifies if the JWT access_token is still valid",
        responses={200: "Token valid"})
    def get(self, request: Request) -> JsonResponse:
        return JsonResponse({"success": "token_valid"}, status=status.HTTP_200_OK)

class RefreshTokenView(APIView):
    """ Delivers new JWT access_token
    :params request.refresh_token: JWT refresh_token
    :returns: HTTP_Response 200 with new JWT access_token if JWT refresh_token is valid, else 401 if JWT refresh_token is None or invalid 
    """
    my_tags = ["Token"]
    authentication_classes = []
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_description="Delivers new JWT access_token",
        responses={200: "new JWT access_token", 401: "JWT refresh"})
    def get(self, request: Request) -> Response:
        refresh_token = request.COOKIES.get('refresh_token')
        if not refresh_token:
            return Response({"error": "refresh_token_missing"}, status=status.HTTP_401_UNAUTHORIZED)
        
        try:
            refresh_token = RefreshToken(refresh_token)
            refresh_token.verify()
            user = User.objects.get(id=refresh_token.payload['user_id'])
            new_refresh_token = RefreshToken.for_user(user)
            access_token = str(new_refresh_token.access_token)
            response = Response({"success": "new_access_token"})
            response.status_code = 200
            response.set_cookie(
                key = settings.SIMPLE_JWT["AUTH_COOKIE"],
                value = access_token,
                expires = settings.SIMPLE_JWT["ACCESS_TOKEN_LIFETIME"],
                secure = settings.SIMPLE_JWT["AUTH_COOKIE_SECURE"],
                httponly = settings.SIMPLE_JWT["AUTH_COOKIE_HTTP_ONLY"],
                samesite = settings.SIMPLE_JWT["AUTH_COOKIE_SAMESITE"]
            )
            response.set_cookie(
                    key = settings.SIMPLE_JWT["REFRESH_COOKIE"],
                    value = str(new_refresh_token),
                    expires = settings.SIMPLE_JWT["REFRESH_TOKEN_LIFETIME"],
                    secure = settings.SIMPLE_JWT["AUTH_COOKIE_SECURE"],
                    httponly = settings.SIMPLE_JWT["AUTH_COOKIE_HTTP_ONLY"],
                    samesite = settings.SIMPLE_JWT["AUTH_COOKIE_SAMESITE"]
                )
            return response
        except:
            return Response({"error": "refresh_token_invalid"}, status=status.HTTP_401_UNAUTHORIZED)



class LogoutView(APIView):
    """ Disconnects one user from API, blacklists the JWT refresh_token
    :params request.refresh_token: JWT refresh_token
    :returns: HTTP_Response 200 if JWT refresh_token is valid, else 401 
    """
    my_tags = ["Authentification"]
    @swagger_auto_schema(
        operation_description="Disconnects one user from API",
        responses={200: "JWT refresh_token blacklisted", 401: "JWT refresh_token not found"}
    )
    def get(self, request: Request) -> Response:
        refresh_token = request.COOKIES.get('refresh_token')
        if refresh_token:
            try:
                RefreshToken(refresh_token).blacklist()
                response = Response({"success": "logout_successfull"}, status=status.HTTP_200_OK)
            except:
                response = Response({"error": "refresh_token_not_found"}, status=status.HTTP_401_UNAUTHORIZED)
        response.delete_cookie("access_token")
        response.delete_cookie("refresh_token")
        return response

class RegisterView(APIView):
    """ Registers one user, create his private group and associate them together
    :params request.firstname: user's firstname
    :params request.lastname: user's lastname
    :params request.password: user's password
    :params request.email: user's email
    :returns: HTTP_Response 200 if success, else 500
    """
    my_tags = ["Authentification"]
    authentication_classes = []
    permission_classes = [AllowAny]
    class RegisterSerializer(serializers.Serializer):
            firstname = serializers.CharField()
            lastname = serializers.CharField()
            password = serializers.CharField()
            email = serializers.EmailField()
            type = serializers.CharField()
    @swagger_auto_schema(
        operation_description="Registers one user, create his private group and associate them together",
        request_body= RegisterSerializer,
        responses={200: "success", 500: "failed"}
    )
    @csrf_exempt
    def post(self, request: Request) -> Response:
        try:
            first_name = request.data["firstname"]
            last_name = request.data["lastname"]
            password = request.data["password"]
            email = request.data["email"]
            type = request.data["type"]
            user = User.objects.create_user(first_name=first_name, last_name=last_name, password=password, email=email, type=type)

            group = Group(public_group=False, owner=user)
            group.save()
            group.create_example_corpus()

            user.groups.add(group.id)
            user.last_active_group = group
            user.is_active = False
            user.save()

            self.send_confirmation_email(request, user, group)
            return Response({"result": True}, status=status.HTTP_200_OK)
        except:
            return Response({"result": False}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    """ Sends a mail to the newest created user with a validation link
    :params user: class User's instance for wich the link will be generated
    :returns: None
    """
    def send_confirmation_email(self, request: Request, user: User, group: Group) -> None:
        current_site = get_current_site(request)
        subject = "Confirmation d'inscription"
        message = render_to_string("confirmation_email.html", {
            "user": user,
            "group": group,
            "domain": current_site.domain,
            "uid": urlsafe_base64_encode(force_bytes(user.pk)),
            "token": default_token_generator.make_token(user),
        })
        send_mail(subject, message, "simon.chauvel@univ-smb.fr", [user.email], fail_silently=False)

# email confirm view
class ConfirmRegistrationView(APIView):
    """ Confirms the user's registration
    :params uidb64: user's id stored in base 64
    :params token: token used to check the request's validity
    :returns: HTTP_Response 200 if ok, else 400
    """
    my_tags = ["Authentification"]
    authentication_classes = []
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_description="Confirms the user's registration",
        responses={200: "success", 400: "failed"}
    )
    def get(self, request: Request, uidb64: str, token: str) -> Response:
        try:
            uid = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            user.is_active = True
            user.save()
            return Response({"result": True}, status=status.HTTP_200_OK)
        else:
            return Response({"result": False}, status=status.HTTP_400_BAD_REQUEST)



# get new password views
class ForgottenPasswordView(APIView):
    """ Finds the requesting user and send him a mail through send_reset_password_email()
    :params request.email: user's email
    :returns: None if user exists, else 404
    """
    my_tags = ["Authentification"]
    authentication_classes = []
    permission_classes = [AllowAny]
    class ForgottenPasswordSerializer(serializers.Serializer):
        email = serializers.EmailField()
    @swagger_auto_schema(
        operation_description="Finds the requesting user and send him a mail through send_reset_password_email()",
        request_body=ForgottenPasswordSerializer,
        responses={200: "success", 404: "failed"}
    )
    def post(self, request: Request) -> Response:
        email = request.data.get("email")
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return Response({"error": "Aucun utilisateur associé à cette adresse e-mail."}, status=status.HTTP_404_NOT_FOUND)
        self.send_reset_password_email(request, user)
        return Response({"message": "Un email de réinitialisation du mot de passe a été envoyé à votre adresse e-mail."}, status=status.HTTP_200_OK)

    """ Sends a mail with encoded user's id and verification token to the user
    :params user: class User's instance
    :returns: None
    """
    def send_reset_password_email(self, request: Request, user: User) -> None:
        current_site = get_current_site(request)
        subject = "Réinitialisation du mot de passe"
        message = render_to_string("reset_password_email.html", {
            "user": user,
            "domain": current_site.domain,
            "uid": urlsafe_base64_encode(force_bytes(user.pk)),
            "token": default_token_generator.make_token(user),
        })
        send_mail(subject, message, "simon.chauvel@univ-smb.fr", [user.email], fail_silently=False)


class ResetPasswordView(APIView):
    """ Defines a new password for user
    :params uidb64: user's id stored in base 64
    :params token: token used to check the request's validity
    :params request.password: new user's password
    :returns: HTTP_Response 200 if new password is set, else 400 if token is invalid
    """
    my_tags = ["Authentification"]
    authentication_classes = []
    permission_classes = [AllowAny]
    class ResetPasswordSerializer(serializers.Serializer):
        password = serializers.CharField()
    @swagger_auto_schema(
        operation_description="Defines a new password for user",
        request_body=ResetPasswordSerializer,
        responses={200: "new password set", 400: "token invalid"}
    )
    def post(self, request: Request, uidb64: str, token: str) -> Response:
        try:
            try:
                uid = force_str(urlsafe_base64_decode(uidb64))
                user = User.objects.get(pk=uid)
            except (TypeError, ValueError, OverflowError, User.DoesNotExist):
                user = None

            if user is not None and default_token_generator.check_token(user, token):
                password = request.data["password"]
                user.set_password(password)
                user.save()
                return Response({"message": "Votre mot de passe a été réinitialisé. Vous pouvez maintenant vous connecter avec votre nouveau mot de passe."}, status=status.HTTP_200_OK)
            else:
                raise Exception
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

class ChangePasswordView(APIView):
    """ Allows a user to change his password if he's connected
    :params request.old_password: user's current password
    :params request.password: user's new password to set
    :returns: HTTP_Response 200 if user is authentified, else 401
    """
    my_tags = ["Authentification"]
    permission_classes = (IsAuthenticated,)
    class ChangePasswordSerializer(serializers.Serializer):
        old_password = serializers.CharField()
        password = serializers.CharField()
    @swagger_auto_schema(
        operation_description="Allows a user to change his password if he's connected",
        request_body=ChangePasswordSerializer,
        responses={200: "password updated", 401: "user unidentified"}
    )
    def post(self, request: Request) -> JsonResponse:
        try:
            old_password = request.data["old_password"]
            user = request.user
            if user.check_password(old_password):
                new_password = request.data["password"]
                user.set_password(new_password)
                user.save()
                update_session_auth_hash(request, user)
                return JsonResponse({"success": "password_updated"}, status=status.HTTP_200_OK)
            else:
                raise Exception
        except:
            return JsonResponse({"error": "user_unidentified"}, status=status.HTTP_401_UNAUTHORIZED)
        


class DeleteAccountView(APIView):
    """ Allows a user to delete his account by sending him a confirmation link through mail
    :params request.password: user's password
    :returns: HTTP_Response 200 if user is authentified, else 401
    """
    my_tags = ["Authentification"]
    permission_classes = (IsAuthenticated,)
    class DeleteAccountSerializer(serializers.Serializer):
        password = serializers.CharField()
        user = serializers.CharField()
    @swagger_auto_schema(
        operation_description="Allows a user to delete his account by sending him a confirmation link through mail",
        request_body=DeleteAccountSerializer,
        responses={200: "account_deletion_request_accepted", 401: "account_deletion_request_refused"}
    )
    def post(self, request: Request) -> Response:
        try:
            password = request.data["password"]
            user = request.user
            if user.check_password(password):
                self.send_account_deletion_validation_email(request, user)
                return Response({"success": "account_deletion_request_accepted"}, status=status.HTTP_200_OK)
            else:
                raise Exception
        except:
            return Response({"error": "account_deletion_request_refused"}, status=status.HTTP_401_UNAUTHORIZED)

    """ Sends a confirmation link through mail
    :params user: class User's instance
    :returns: None
    """
    def send_account_deletion_validation_email(self, request: Request, user: User) -> None:
        current_site = get_current_site(request)
        subject = "Validation de suppression de compte"
        message = render_to_string("account_deletion_validation_email.html", {
            "user": user,
            "domain": current_site.domain,
            "uid": urlsafe_base64_encode(force_bytes(user.pk)),
            "token": default_token_generator.make_token(user),
        })
        send_mail(subject, message, "simon.chauvel@univ-smb.fr", [user.email], fail_silently=False)


class ConfirmationDeleteAccountView(APIView):
    """ Confirms user's account deletion and send him confirmation through mail
    :params uidb64: user's id stored in base 64
    :params token: token used to check the request's validity
    :returns: HTTP_Response 400 if token invalid
    """
    my_tags = ["Authentification"]
    authentication_classes = []
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_description="Confirms user's account deletion and send him confirmation through mail",
        responses={200: "account_deleted", 400: "invalid_token"}
    )
    def delete(self, request: Request, uidb64: str, token: str) -> Response:
        try:
            try:
                uid = force_str(urlsafe_base64_decode(uidb64))
                user = User.objects.get(pk=uid)
            except:
                user = None
            if user is not None and default_token_generator.check_token(user, token):
                user.delete()
                self.send_account_deleted_confirmation_email(request, user)
                return Response({"success": "Votre compte a été supprimé."}, status=status.HTTP_200_OK)
            else:
                raise Exception
        except:
            return Response({"error": "Le lien de suppression du compte est invalide ou a expiré."}, status=status.HTTP_400_BAD_REQUEST)

    """ Sends deletion confirmation mail to user
    :params user: class User's instance
    :returns: None
    """
    def send_account_deleted_confirmation_email(self, request: Request, user: User) -> None:
        current_site = get_current_site(request)
        subject = "Compte supprimé à votre demande"
        message = render_to_string("account_deleted_validation_email.html", {
            "user": user,
            "domain": current_site.domain,
        })
        send_mail(subject, message, "simon.chauvel@univ-smb.fr", [user.email], fail_silently=False)


class Me(APIView):
    """ Gets connected user's informations
    :params request.user: class User's instance (if user is connected)
    :returns: HTTP_Response 200 with user's information (mail, fullname, type, is_superuser), else 401
    """

    my_tags = ["User"]
    authentication_classes = [CustomJWTAuth]     
    @swagger_auto_schema(
        operation_description="Gets connected user's informations",
        responses={200: "user's informations", 401: "failed"}
    )
    
    def get(self, request: Request) -> JsonResponse:
        try:
            return JsonResponse({"email": request.user.email, "firstname": request.user.first_name, "lastname": request.user.last_name, "type": request.user.type, "is_superuser": request.user.is_superuser, "mail_agreement": request.user.mail_agreement}, status=status.HTTP_200_OK)
        except:
            return JsonResponse({"error": "failed"}, status=status.HTTP_401_UNAUTHORIZED)

    """ Allows a user to modify his personnal informations
    :params request.user: class User's instance
    :params request.firstname: user's new firstname
    :params request.lastname: user's new lastname
    :params request.email: user's new email
    :params request.type: user's new type
    :params request.workGroup: user's new workGroup
    :returns: HTTP_Response 200 if update, else 401
    """
    
    class MeSerializer(serializers.Serializer):
        firstname = serializers.CharField()
        lastname = serializers.CharField()
        email = serializers.EmailField()
        type = serializers.CharField()
        workGroup = serializers.CharField()
        agreement = serializers.BooleanField()
    
    permission_classes = (IsAuthenticated,)
    @swagger_auto_schema(
        operation_description="Allows a user to modify his personnal informations",
        request_body=MeSerializer,
        responses={200: "user_updated", 401: "failed"}
    )
    def post(self, request: Request) -> JsonResponse:
        try:
            user = request.user
            kwargs = {
                "first_name": request.data.get("firstname"),
                "last_name": request.data.get("lastname"),
                "email": request.data.get("email"),
                "type": request.data.get("type"),
                "last_active_group_id": request.data.get("workGroup"),
                "mail_agreement": request.data.get("agreement"),
            }
            user.update(**kwargs)
            return JsonResponse({"success": "user_updated"}, status=status.HTTP_200_OK)
        except:
            return JsonResponse({"error": "failed"}, status=status.HTTP_401_UNAUTHORIZED)


class Groups(APIView):
    """ Gets all user's groups and public groups
    :params request.user: class User's instance
    :returns: HTTP_Response 200 with user's groups data and public groups data if success, else 401
    """
    my_tags = ["Group"]
    @swagger_auto_schema(
        operation_description="Gets all user's groups and public groups",
        responses={200: "user's groups data and public groups data", 401: "failed"}
    )
    @csrf_exempt
    def get(self, request: Request) -> JsonResponse:
        groups = request.user.groups.all().order_by("name")
        public_groups = Group.objects.filter(public_group=True).order_by("name")
        res = []
        res_public = []
        try:
            for group in groups:
                group_members = group.users.all().order_by("last_name")
                res.append({
                    "id": group.id,
                    "name": group.name,
                    "members": [member.get_full_name() for member in group_members],
                    "owner": group.verify_owner(request.user),
                    "status": group.public_group,
                    "active": group.is_active(request.user),
                })
            for group in public_groups:
                if group not in groups:
                    res_public.append({
                        "id": group.id,
                        "name": group.name,
                    })
            return JsonResponse({"groups": res, "public_groups": res_public}, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    """ Creates a new group
    :params request.group_name: new group's name
    :params request.group_users: new group's members
    :params request.user: class User's instance, group's owner
    :params request.group_status: group's status either private nor public
    :returns: HTTP_Response 201 if success, else 401
    """
    class GroupsSerializer(serializers.Serializer):
        group_name = serializers.CharField()
        group_users = serializers.ListField()
        group_status = serializers.BooleanField()
    @swagger_auto_schema(
        operation_description="Creates a new group",
        request_body=GroupsSerializer,
        responses={201: "group_created", 401: "failed"}
    )
    def post(self, request: Request) -> JsonResponse:
        try:
            group_name = request.data.get("group_name")
            group_users = [request.data.get("group_users")]
            group_users.append(request.user.email)
            group_status = request.data.get("group_status")
            group = Group(name=group_name, owner=request.user, public_group=group_status)
            group.save()
            for user_email in group_users:
                try:
                    user = User.objects.get(email=user_email)
                except:
                    continue
                group.users.add(user)
                if group.verify_owner(user):
                    user_notify(request=self.request, user=user, message=f"GP_create_<{group.name}>", subject="Création de groupe", template="group_created_email.html")
                else:
                    user_notify(request=self.request, user=user, message=f"GP_add_<{group.name}>", subject="Ajout à un groupe", template="user_add_to_group_email.html")

            return JsonResponse({"success": "group_created"}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=status.HTTP_401_UNAUTHORIZED)

    """ Either updates an existing group (if user owns the group) nor adds or removes current user to a group
    :params group_id: group's id to be updated
    :params request.user: class User's instance
    :params request.group_name: new group's name
    :params request.group_status: new group's status, either True (public) nor False (private)
    :params request.group_member: either user's email (user to add) nor user's fullname (user to remove) != request.user
    :returns: HTTP_Response 200 if all ok, else 401 if user tries to leave his own group, else 406 if user is already in group, else 404 if user does not exist
    """
    class PutGroupsSerializer(serializers.Serializer):
        group_name = serializers.CharField()
        group_status = serializers.BooleanField()
        group_member = serializers.CharField()
    @swagger_auto_schema(
        operation_description="Either updates an existing group (if user owns the group) nor adds or removes current user to a group",
        request_body=PutGroupsSerializer,
        responses={200: "group_updated", 401: "cannot_leave_owned_group", 406: "user_already_in_group", 404: "user_unknown"}
    )
    def put(self, request: Request, group_id: str) -> JsonResponse:
        try:
            group = Group.objects.get(id=group_id)
            if (len(request.data) <= 0):
                if not group.verify_member(request.user):
                    request.user.groups.add("group", group)
                    request.user.save()
                    user_notify(request=request, user=request.user, message=f"GP_join_<{group.name}>", subject="Groupe rejoint", template="join_group_email.html")
                    return JsonResponse({"success": "group_updated", "user": request.user.get_full_name()}, status=status.HTTP_200_OK)
                else:
                    request.user.groups.remove(group)
                    request.user.save()
                    user_notify(request=request, user=request.user, message=f"GP_leave_<{group.name}>", subject="Groupe quitté", template="leave_group_email.html")
                    return JsonResponse({"success": "group_updated"}, status=status.HTTP_200_OK)
            kwargs = {
                "name": request.data.get("group_name"),
                "add_user": None,
                "remove_user": None,
                "public_group": request.data.get("group_status")
            }
            if request.data.get("group_member") is not None and re.match(r"\S+[@]\w+.\w+", request.data.get("group_member")):
                kwargs["add_user"] = User.objects.get(email=request.data.get("group_member")) 
            elif request.data.get("group_member") is not None and re.match(r"\S+\s\S+", request.data.get("group_member")):
                kwargs["remove_user"] = User.objects.get(first_name=request.data.get("group_member").split(" ")[0], last_name=request.data.get("group_member").split(" ")[1])

            if request.user not in [kwargs["add_user"], kwargs["remove_user"]]:
                if kwargs["add_user"] in group.users.all():
                    return JsonResponse({"error": "user_already_in_group"}, status=status.HTTP_406_NOT_ACCEPTABLE)
                
                group.update(**kwargs)
                
                if kwargs["add_user"]:
                    user_notify(request=request, user=kwargs["add_user"], message=f"GP_add_<{group.name}>", subject="Ajout à un groupe", template="add_group_email.html")
                elif kwargs["remove_user"]:
                    user_notify(request=request, user=kwargs["remove_user"], message=f"GP_ban_<{group.name}>", subject="Exclusion d'un groupe", template="ban_group_email.html")
                    
                return JsonResponse({"success": "group_updated", "user": kwargs["add_user"].get_full_name() if kwargs["add_user"] is not None else None}, status=status.HTTP_200_OK)
                
            else:
                return JsonResponse({"error": "cannot_leave_owned_group"}, status=status.HTTP_401_UNAUTHORIZED)
        except ObjectDoesNotExist:
            return JsonResponse({"error": "user_unknown"}, status=status.HTTP_404_NOT_FOUND)
        except:
            return JsonResponse({"error": "failed"}, status=status.HTTP_401_UNAUTHORIZED)
                
    """ Allows user to delete owned group
    :params group_id: group's id to be deleted
    :params request.user: class User's instance
    :returns: HTTP_Response 204 if success, else 401 if user tries to delete his last group or user tries to delete unowned group
    """
    @swagger_auto_schema(
        operation_description="Allows user to delete owned group",
        responses={204: "group_deleted", 401: "last_group_cant_be_deleted"}
    )
    def delete(self, request: Request, group_id: str) -> JsonResponse:
        try:
            group = Group.objects.get(id=group_id)
            owned_groups = request.user.get_owned_groups()
            if group.verify_owner(request.user):
                if len(owned_groups) > 1:
                    users = group.users.all()
                    for user in users:
                        if group.is_active(user):
                            user.last_active_group = user.groups.exclude(id=group_id).first()
                            user.save()
                        user_notify(request=self.request, user=user, message=f"GP_del_<{group.name}>", subject="Suppression de groupe", template="group_delete_email.html")
                    group.delete()
                    return JsonResponse({"success": "group_deleted"}, status=status.HTTP_204_NO_CONTENT)
                else:
                    return JsonResponse({"error": "last_group_cant_be_deleted"}, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return JsonResponse({"error": "permission_denied"}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
