from django.http import JsonResponse
from rest_framework_simplejwt.authentication import JWTAuthentication

class JWTMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        jwt_authentication = JWTAuthentication()
        user, jwt_token = jwt_authentication.authenticate(request)

        if user is None or not jwt_token:
            return JsonResponse({"error": "Unauthorized"}, status=401)

        response = self.get_response(request)
        return response
