from authentication.models import User
from scraping_handler.models import ScrapingResult
from Neo4j.models import Neo4j_Proxyma_Corpus
from django.test import TestCase
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework.test import APIRequestFactory
from unittest.mock import patch
import json

class PlugViewTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)

	def test_post_200(self):
		with patch("scraping_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			data = [{}]
			json_data = json.dumps(data)
			uploaded_file = SimpleUploadedFile("data.json", bytes(json_data, "utf-8"), content_type="application/json")
			response = self.client.post(reverse("scraping_handler:plugview"), {"data": uploaded_file})
			self.assertEqual(response.status_code, 200)

	def test_post_500(self):
		with patch("scraping_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			response = self.client.post(reverse("scraping_handler:plugview"))
			self.assertEqual(response.status_code, 500)

	def test_post_401(self):
		with patch("scraping_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("scraping_handler:plugview"))
			self.assertEqual(response.status_code, 401)

class CorpusNamesTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		ScrapingResult(mapTitle="test", initial_corpus=[{}], processed_corpus=[{}], user=self.user).save()
		self.scrapingResult = ScrapingResult.objects.get(mapTitle="test")

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.get(reverse("scraping_handler:corpusnames"))
		self.assertEqual(response.status_code, 200)
		self.assertIn({"id": str(self.scrapingResult.uid), "title": self.scrapingResult.mapTitle}, response.json())
		
	def test_get_401(self):
		response = self.client.get(reverse("scraping_handler:corpusnames"))
		self.assertEqual(response.status_code, 401)

class CorpusHandlerTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		ScrapingResult(mapTitle="test", initial_corpus=[{}], processed_corpus=[{}], user=self.user).save()
		self.scrapingResult = ScrapingResult.objects.get(mapTitle="test")

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.get(reverse("scraping_handler:getcorpus", kwargs={"corpus_id": self.scrapingResult.uid}))
		self.assertEqual(response.status_code, 200)
		self.assertEqual({"corpus": self.scrapingResult.mapTitle,"id": str(self.scrapingResult.uid), "data": self.scrapingResult.processed_corpus[0]}, response.json())
	
	def test_get_401(self):
		response = self.client.get(reverse("scraping_handler:getcorpus", kwargs={"corpus_id": self.scrapingResult.uid}))
		self.assertEqual(response.status_code, 401)

	def test_get_500(self):		
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.get(reverse("scraping_handler:getcorpus", kwargs={"corpus_id": "1234"}))
		self.assertEqual(response.status_code, 500)

class ValidateActivityTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		self.initial_corpus = [
							{
								"corpus": "tests BeatCorp",
								"activities": [
									{
										"tasks": [
											{
												"date": "Tâche terminée le  30 janvier 2024 à 13:13 par Simon Chauvel",
												"task": "Scraper des données pour tester l'algo de data analyse",
												"actors": ["Simon Chauvel"],
												"actions": [],
												"details": [
													"Objectif : créer une carte\n",
													"Ressource : API Django et Spacy\n",
													"Ressource : navigateur et extension de scraping\n"
												]
											}
										],
										"activity": "part1"
									}
								]
							}
						]

		self.processed_corpus = [
									{
										"corpus": "tests BeatCorp",
										"activities": [
											{
												"index": 0,
												"tasks": [
													{
														"date": "2024-01-30",
														"index": 0,
														"actors": ["Simon Chauvel"],
														"actions": ["Scraper des données"],
														"validated": True,
														"personnal_resources": [
															{ "type": "k", "description": "HTML & CSS" },
															{
																"type": "k",
																"description": "Javascript/Typescript"
															},
															{ "type": "k", "description": "Python/Django" },
															{ "type": "k", "description": "API rest" },
															{ "type": "s", "description": "développement Web" }
														],
														"environmental_resources": [
															{ "type": "logiciel", "description": "API Django" },
															{
																"type": "logiciel",
																"description": "extension de scraping"
															},
															{ "type": "logiciel", "description": "SPacy" }
														]
													}
												],
												"schemes": [
													"collecte, nettoyage et enrichissement d'un set de données"
												],
												"activity": "Récolte d'un set de données",
												"situation": "projet CaTCaP",
												"objectives": [
													"cartographier un set de données",
													"récolter des données"
												],
												"performances": [
													{
														"unit": "%",
														"value": "75",
														"criterion": "données scrapées / données cartographiées"
													}
												],
												"personnal_resources": []
											}
										]
									}
								]

		ScrapingResult(mapTitle="test", initial_corpus=self.initial_corpus, processed_corpus=self.processed_corpus, user=self.user).save()
		self.scrapingResult = ScrapingResult.objects.get(mapTitle="test")

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.get(reverse("scraping_handler:premap", kwargs={"corpus_title": self.scrapingResult.mapTitle, "corpus_id": str(self.scrapingResult.uid)}))
		self.assertEqual(response.status_code, 200)
		proxyma = response.json()["proxyma"]
		data = response.json()["data"]
		self.assertEqual(type(proxyma), str)
		self.assertEqual(type(data), list)

	def test_get_401(self):
		response = self.client.get(reverse("scraping_handler:premap", kwargs={"corpus_title": self.scrapingResult.mapTitle, "corpus_id": str(self.scrapingResult.uid)}))
		self.assertEqual(response.status_code, 401)

	def test_delete_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.delete(reverse("scraping_handler:deletecorpus", kwargs={"corpus_id": str(self.scrapingResult.uid)}))
		self.assertEqual(response.status_code, 200)
		with self.assertRaises(ObjectDoesNotExist):
			ScrapingResult.objects.get(mapTitle="test")

	def test_delete_401(self):
		response = self.client.delete(reverse("scraping_handler:deletecorpus", kwargs={"corpus_id": str(self.scrapingResult.uid)}))
		self.assertEqual(response.status_code, 401)

	def test_delete_500(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.delete(reverse("scraping_handler:deletecorpus", kwargs={"corpus_id": "1234"}))
		self.assertEqual(response.status_code, 500)
		self.assertEqual(ScrapingResult.objects.get(mapTitle="test"), self.scrapingResult)
		
	def test_post_200(self):
		with patch("scraping_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })

			activity = {
				"activity": "Récolte d'un set de données",
				"situation": "projet CaTCaP",
				"objectives": [
					"cartographier un set de données",
					"récolter des données"
				],
				"schemes": [
					"collecte, nettoyage et enrichissement d'un set de données"
				],
				"performances": [
					{
						"unit": "%",
						"value": "75",
						"criterion": "données scrapées / données cartographiées"
					}
				],
				"personnal_resources": [],
				"task": {
					"actors": ["Simon Chauvel"],
					"actions": ["Scraper des données"],
					"personnal_resources": [
						{ "type": "k", "description": "HTML & CSS" },
						{
							"type": "k",
							"description": "Javascript/Typescript"
						},
						{ "type": "k", "description": "Python/Django" },
						{ "type": "k", "description": "API rest" },
						{ "type": "s", "description": "développement Web" }
					],
					"environmental_resources": [
						{ "type": "logiciel", "description": "API Django" },
						{
							"type": "logiciel",
							"description": "extension de scraping"
						},
						{ "type": "logiciel", "description": "SPacy" }
					]
				}
			}

			json_data = json.dumps(activity)
			uploaded_file = SimpleUploadedFile("work_corpus.json", bytes(json_data, "utf-8"), content_type="application/json")

			proxyma = Neo4j_Proxyma_Corpus(uid=self.scrapingResult.uid, title="test").save()

			response = self.client.post(reverse("scraping_handler:validateactivity", kwargs={"corpus_id": str(self.scrapingResult.uid), "activityIndex": 0, "taskIndex": 0}), { "work_corpus": uploaded_file, "file": uploaded_file })
			self.assertEqual(response.status_code, 200)
			proxyma.delete()

	def test_post_500(self):
		with patch("scraping_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			uploaded_file = []
			response = self.client.post(reverse("scraping_handler:validateactivity", kwargs={"corpus_id": str(self.scrapingResult.uid), "activityIndex": 0, "taskIndex": 0}), { "work_corpus": uploaded_file, "file": uploaded_file })
			self.assertEqual(response.status_code, 500)

	def test_post_401(self):
		with patch("scraping_handler.views.threading.Thread.start", return_value=None):
			uploaded_file = []
			response = self.client.post(reverse("scraping_handler:validateactivity", kwargs={"corpus_id": str(self.scrapingResult.uid), "activityIndex": 0, "taskIndex": 0}), { "work_corpus": uploaded_file, "file": uploaded_file })
			self.assertEqual(response.status_code, 401)

class GetRawDataTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		ScrapingResult(mapTitle="test", initial_corpus=[{}], processed_corpus=[{}], user=self.user).save()
		self.scrapingResult = ScrapingResult.objects.get(mapTitle="test")

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.get(reverse("scraping_handler:getrawdata", kwargs={"corpus_id": self.scrapingResult.uid}))
		self.assertEqual(response.status_code, 200)
		pre_process_data = response.json()["pre_process_data"]
		post_process_data = response.json()["post_process_data"]
		self.assertEqual(type(pre_process_data), list)
		self.assertEqual(type(post_process_data), list)

	def test_get_500(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.get(reverse("scraping_handler:getrawdata", kwargs={"corpus_id": "1234"}))
		self.assertEqual(response.status_code, 500)

	def test_get_401(self):
		response = self.client.get(reverse("scraping_handler:getrawdata", kwargs={"corpus_id": self.scrapingResult.uid}))
		self.assertEqual(response.status_code, 401)
