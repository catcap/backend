from django.urls import path
from scraping_handler.views import *

app_name = "scraping_handler"

urlpatterns = [
    path('handle_scraping', PlugView.as_view(), name='plugview'),
    path('getCorpusNames', CorpusNames.as_view(), name='corpusnames'),
    path('getCorpus/<str:corpus_id>', CorpusHandler.as_view(), name='getcorpus'),
    path('preMap/<str:corpus_title>/<str:corpus_id>', ValidateActivity.as_view(), name='premap'),
    path('deleteCorpus/<str:corpus_id>', ValidateActivity.as_view(), name='deletecorpus'),
    path('validateActivity/<str:corpus_id>/<str:activityIndex>/<str:taskIndex>', ValidateActivity.as_view(), name="validateactivity"),
    path('rawData/<str:corpus_id>', GetRawData.as_view(), name="getrawdata"),
]