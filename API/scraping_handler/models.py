from django.db import models
import uuid, copy
from authentication.models.User import User

class ScrapingResult(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    mapTitle = models.CharField(max_length=150, null=True)
    initial_corpus = models.JSONField(null=True)
    processed_corpus = models.JSONField(null=True)

    #Relations
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="ScrapResults", null=False)

    def get_unvalidated(self):
        result = self.processed_corpus[0]
        updated_activities = []
        for activity in result["activities"]:
            updated_tasks = []
            for task in activity["tasks"]:
                if not task["validated"]:
                    updated_tasks.append(task)
            if updated_tasks:
                activity["tasks"] = updated_tasks
                updated_activities.append(activity)

        result["activities"] = updated_activities
        return result

    def validate_task(self, activityIndex, taskIndex, work_corpus):
        for i, elem in enumerate(self.processed_corpus[0]["activities"]):
            if elem["index"] == int(activityIndex):
                elem["activity"] = work_corpus["activity"]
                elem["objectives"] = work_corpus["objectives"]
                elem["performances"] = work_corpus["performances"]
                elem["schemes"] = work_corpus["schemes"]
                elem["situation"] = work_corpus["situation"]
                for task in elem["tasks"]:
                    if task["index"] == int(taskIndex):
                        task["actors"] = work_corpus["task"]["actors"]
                        task["actions"] = work_corpus["task"]["actions"]
                        task["environmental_resources"] = work_corpus["task"]["environmental_resources"]
                        task["personnal_resources"] = work_corpus["task"]["personnal_resources"]
                        task['validated'] = True
                        self.save()
        return self.processed_corpus