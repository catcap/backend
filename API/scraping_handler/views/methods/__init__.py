from scraping_handler.views.methods.define_objective import *
from scraping_handler.views.methods.details_parser import *
from scraping_handler.views.methods.data_write_neo4j import *
from scraping_handler.views.methods.define_date import *