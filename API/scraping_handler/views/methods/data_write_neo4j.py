from Neo4j.models import *

def actions_writer(actions):
    db_actions = []
    for index, action in enumerate(actions):
        if action:
            db_action = Neo4j_Action(description=action).save()
            db_actions.append(db_action)
    return db_actions

def activity_writer(activity):
    if activity:
        db_activity = Neo4j_Activity(description=activity).save()
        return db_activity

def actors_writer(actors):
    db_actors = []
    for index, actor in enumerate(actors):
        if actor:
            try:
                db_actor = Neo4j_Actor.nodes.get(name=actor)
            except:
                db_actor = Neo4j_Actor(name=actor).save()
            db_actors.append(db_actor)
    return db_actors

def environmental_resources_writer(environmental_resources):
    db_environmental_resources = []
    for index, environmental_resource in enumerate(environmental_resources):
        if environmental_resource:
            db_environmental_resource = Neo4j_Environmental_Resource(description=environmental_resource).save()
            db_environmental_resources.append(db_environmental_resource)
    return db_environmental_resources

def objective_writer(objective):
    if objective:
        db_objective = Neo4j_Objective(description=objective).save()
        return db_objective

def performance_writer(performance, effectiveness):
    if performance and effectiveness:
        db_performance = Neo4j_Performance(performance=performance, effectiveness=effectiveness).save()
        return db_performance

def personnal_resources_writer(personnal_resources):
    db_personnal_resources = []
    for personnal_resource in personnal_resources:
        if personnal_resource:
            db_personnal_resource = Neo4j_Personnal_Resource(type=personnal_resource["type"], description=personnal_resource["description"]).save()
            db_personnal_resources.append(db_personnal_resource)
    return db_personnal_resources

def proxyma_corpus_writer(proxyma_corpus, corpus_timestamp, group):
    try:
        db_proxyma_corpus = Neo4j_Proxyma_Corpus.nodes.get(title=proxyma_corpus, group=group, is_validated=False)
    except:
        db_proxyma_corpus = Neo4j_Proxyma_Corpus(title=proxyma_corpus, corpus_timestamp=corpus_timestamp, group=group, is_validated=False).save()
    return db_proxyma_corpus

def scheme_writer(scheme):
    if scheme:
        db_scheme = Neo4j_Scheme(actions_sequence=scheme).save()
    return db_scheme

def situation_writer(situation):
    if situation:
        db_situation = Neo4j_Situation(description=situation).save()
    return db_situation



def connect_corpus_to_actions(corpus, actions):
    if len(actions):
        for action in actions:
            corpus.action.connect(action)

def connect_corpus_to_activity(corpus, activity):
    if activity:
        corpus.activity.connect(activity)

def connect_corpus_to_actors(corpus, actors):
    if len(actors):
        for actor in actors:
            corpus.actor.connect(actor)

def connect_corpus_to_environmental_resources(corpus, environmental_resources):
    if len(environmental_resources):
        for environmental_resource in environmental_resources:
            corpus.environmental_resource.connect(environmental_resource)

def connect_corpus_to_objective(corpus, objective):
    corpus.objective.connect(objective)

def connect_corpus_to_performance(corpus, performance):
    if performance:
        corpus.performance.connect(performance)

def connect_corpus_to_personnal_resources(corpus, personnal_resources):
    if len(personnal_resources):
        for personnal_resource in personnal_resources:
            corpus.personnal_resource.connect(personnal_resource)

def connect_corpus_to_scheme(corpus, scheme):
    if scheme:
        corpus.scheme.connect(scheme)

def connect_corpus_to_situation(corpus, situation):
    if situation:
        corpus.situation.connect(situation)




def connect_activity_to_objective(activity, objective):
    activity.objective.connect(objective)

def connect_activity_to_scheme(activity, scheme):
    activity.scheme.connect(scheme)

def connect_activity_to_situation(activity, situation):
    activity.situation.connect(situation)

def connect_actor_to_personnal_resources(actor, personnal_resources):
    for personnal_resource in personnal_resources:
        actor.personnal_resource.connect(personnal_resource)

def connect_objective_to_performance(objective, performance):
    objective.performance.connect(performance)

def connect_scheme_to_actions(scheme, actions):
    for action in actions:
        scheme.action.connect(action)

def connect_scheme_to_actors(scheme, actors):
    for actor in actors:
        scheme.actor.connect(actor)
    
def connect_scheme_to_environmental_resources(scheme, environmental_resources):
    for environmental_resource in environmental_resources:
        scheme.environmental_resource.connect(environmental_resource)

