import re, spacy, copy
from typing import TypedDict
nlp = spacy.load('fr_core_news_sm')


class Result(TypedDict):
	resources: list[str]
	objectives: list[str]

# conversion
def details_cleaner(details: list[str]) -> list[str]:
	splited_details = copy.copy(details)
	details = []
	for detail in splited_details:
		doc = nlp(detail)
		unwanted_words = r"\bRes?sources?|Objecti(ves?|fs?)\b"
		filtered_tokens = [token.text for token in doc if not token.is_punct and not re.match(unwanted_words, token.text, re.IGNORECASE)]
		filtered_text = " ".join(filtered_tokens).split("\n")
		for text in filtered_text:
			if not text == "":
				text = text.strip()
				details.append(text)
	return details

# formatting
def extract_resources_and_objectives(details: list[str]) -> Result:
	resources = []
	objectives = []
	for detail in details:
		doc = nlp(detail)
		is_objective = any(token.pos_ == "VERB" and token.text == token.lemma_ for token in doc)
		is_only_noun = all(token.pos_ == "PROPN" or token.pos_ == "NOUN" for token in doc)
		if is_objective:
			objectives.append(doc.text)
		elif is_only_noun:
			for token in doc:
				resources.append(doc.text)
		else:
			last_punct = None
			for i, token in enumerate(doc):
				if last_punct is None and token.pos_ == "PUNCT":
					resources.append(doc[:i].text)
					last_punct = i
				elif last_punct is not None and token.pos_ == "PUNCT":
					resources.append(doc[last_punct+1:i].text)
					last_punct = i
				elif i >= (len(doc)-1):
					resources.append(doc[:i+1].text)

	return Result(resources=list(set(resources)), objectives=objectives)


def details_parser(details: list[str]) -> Result:
	details = details_cleaner(details)
	result = extract_resources_and_objectives(details)
	return result
