import calendar, datetime, locale, re
locale.setlocale(locale.LC_ALL, "")

from django.utils import timezone

""" Builds a date in format Y-m-d
:params date: a string wich contains the date to retrieves
:returns: a string in format Y-m-d
"""
def define_date(date: str) -> str:
    date = get_date_format(date)
    match date['format']:
        case "beesbusy_date":
            day = int(date['groups']['day'])
            for i, m in enumerate(calendar.month_name):
                if date['groups']['month'] == m:
                    month = i
            year = int(date['groups']['year'])
        case "notion_date":
            day = int(date['groups']['day'])
            month = int(date['groups']['month'])
            year = int(date['groups']['year'])
        case "trello_last_week":
            current_weekday = calendar.day_name.index(date['groups']['day'].title())
            last_week = datetime.now() - datetime.timedelta(days=7)
            day = (last_week - datetime.timedelta(days=last_week.weekday() - current_weekday)).day
            month = datetime.datetime.now().month
            year = datetime.datetime.now().year
        case "trello_next_week":
            current_weekday = calendar.day_name.index(date['groups']['day'].title())
            next_week = datetime.now() + datetime.timedelta(days=7)
            day = (next_week - datetime.timedelta(days=next_week.weekday() - current_weekday)).day
            month = datetime.datetime.now().month
            year = datetime.datetime.now().year
        case "trello_current_days":
            match date["groups"]["day"]:
                case "hier":
                    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
                    day = yesterday.day
                    month = yesterday.month
                    year = yesterday.year
                case "aujourd'hui":
                    day = datetime.datetime.now().day
                    month = datetime.datetime.now().month
                    year = datetime.datetime.now().year
                case "demain":
                    tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
                    day = tomorrow.day
                    month = tomorrow.month
                    year = tomorrow.year
        case "trello_current_year" | "trello_with_deadline_current_year":
            day = int(date["groups"]["day"])
            for i, m in enumerate(calendar.month_name):
                if m.startswith(date['groups']['month'].replace(".", "")):
                    month = i
            year = datetime.datetime.now().year
        case "trello_other_year" | "trello_with_deadline_other_year":
            day = int(date["groups"]["day"])
            for i, m in enumerate(calendar.month_name):
                if m.startswith(date['groups']['month'].replace(".", "")):
                    month = i
            year = int(date["groups"]["year"])
        case "kbskill_date":
            day = int(date["groups"]["day"])
            month = int(date["groups"]["month"])
            year = int(date["groups"]["year"])
        case _:
            day = datetime.datetime.now().day
            month = datetime.datetime.now().month
            year = datetime.datetime.now().year

    date = timezone.make_aware(datetime.datetime(year, month, day, 0, 0))
    date = date.strftime('%Y-%m-%d')
    return date

""" Parses a string to find a date's parts and its format using regex
:params date: a string wich contains the date to parse
:returns: a dict containing the date's format and the captured groups
"""
def get_date_format(date: str) -> str:
    date_formats = [
        (r"(?P<day>\b\d+\b)\s(?P<month>\b\w+\b)\s(?P<year>\b\d+\b).*?(?P<time>\b([01]\d|2[0-3])([:]?)([0-5]\d)\b)", "beesbusy_date"),
        (r"(?:\b(?P<day>(?:0[1-9]|[12]\d|3[01]))\b)/(?:\b(?P<month>(?:0[1-9]|1[0-2]))\b)/(?P<year>\b\d{4}\b)", "notion_date"),
        (r"\b(?P<day>(lundi|mardi|mercredi|jeudi|vendredi|samedi|dimanche)) dernier\b", "trello_last_week"),
        (r"\b(?P<day>(lundi|mardi|mercredi|jeudi|vendredi|samedi|dimanche))\b", "trello_next_week"),
        (r"\b(?P<day>(hier|aujourd'hui|demain))\b", "trello_current_days"),
        (r"\b(?P<day>(\d{1,2}))\s(?P<month>([a-zA-Z]{3,4}))\b", "trello_current_year"),
        (r"\b(?P<day>(\d{1,2}))\s(?P<month>([a-zA-Z]{3,4}))\s(?P<year>(\d{4}))\b", "trello_other_year"),
        (r"\b(?P<day>\d{1,2})\s(?P<month>[a-zA-Z]{3,4})\b", "trello_with_deadline_current_year"),
        (r"\b(?P<day>\d{1,2})\s(?P<month>[a-zA-Z]{3,4})\s(?P<year>(\d{4}))\b", "trello_with_deadline_other_year"),
        (r"(?P<month>\b\d{2})\/(?P<day>\b\d{2})\/(?P<year>\b\d{4})", "kbskill_date"),
    ]
    for regex, format_str in date_formats:
        match = re.search(regex, date, re.IGNORECASE)
        if match:
            groups = match.groupdict()
            return {
                "format": format_str,
                "groups": groups,
            }
    return None