import spacy, re
from typing import TypedDict
nlp = spacy.load('fr_core_news_sm')

class Result(TypedDict):
	action: str
	objective: str|None

""" Parses a string with spacy, finds if any words of it matches any keywords dictionary key, then find the proper variant in the key's associated array to defines the action part and objective part of the sentence (assuming that objective start with a verb)
:params sentence: a string to analyse
:returns: a dictionary containing two keys : action and objective, if no objective found, objective is set to None
"""
def define_objective(sentence: str) -> Result:
	doc = nlp(sentence)
	
	keywords = {
		'objectif': ['dans l\'objectif de', 'avec pour objectif de', 'en ayant pour objectif de', 'en ayant pour objectif de', 'dans l’objectif de'],
		'atteindre': ['pour atteindre', 'en vue d\'atteindre'],
		'finalité': ['en ayant pour finalité de', 'avec pour finalité de'],
		'but': ['dans le but de', 'avec pour but de'],
		'pour': [ 'pour que', 'pour'],
		'afin': ['afin de', 'afin d\'', 'afin que'],
		'vue': ['en vue de', 'en vue d\’atteindre', 'en vue de réaliser', 'en vue d\'accomplir'],
		'sorte': ['de sorte que'],
		'visant': ['en visant à'],
		'intention': ['dans l\'intention de'],
		'dessein': ['dans le dessein de'],
		'cherchant': ['en cherchant à']
	}

	root = None
	variant = None
	last_word = None
	action = None
	objective = None
	for i, token in enumerate(doc):
		for key in keywords:
			if token.text == key:
				root = token
				for var in keywords[root.text]:
					if var in doc.text:
						variant = var
						last_word = var.split()[-1]
						break
			if root is not None:
				break
		if token.text == last_word:
			for j, word in enumerate(doc):
				if j <= i:
					continue
				if word.pos_ == "VERB":
					action = doc[:j-1]
					objective = doc[j:]
					break
			

	if objective is not None:
		print(Result(action=sentence.split(variant)[0], objective=sentence.split(variant)[1]))
		return Result(action=sentence.split(variant)[0], objective=sentence.split(variant)[1])
	else:
		return Result(action=sentence, objective=None)
