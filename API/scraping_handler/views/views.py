import json, threading, uuid, requests, copy, datetime


from io import StringIO

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.request import Request
from authentication.authenticate import CustomJWTAuth
from scraping_handler.views.methods import *
from scraping_handler.models import ScrapingResult
from neomodel import db, DoesNotExist
from authentication.models import User
from Neo4j.models import *
from WS_notifs.user_notify import user_notify

from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers


class PlugView(APIView):
    my_tags = ["Scraping"]
    
    """ Retrieves scraped data from Web-browser extension and treat them through an async thread
    :params request.user: class User's instance, the requesting user
    :params request.data: scraped data, .json format
    :returns: HTTP_Response 200 if the thread runs fine, else 500
    """
    authentication_classes = [CustomJWTAuth]
    permission_classes = [IsAuthenticated]
    
    class PlugSerializer(serializers.Serializer):
        data = serializers.FileField()
    @swagger_auto_schema(
        operation_summary="Plug data",
        request_body=PlugSerializer,
        responses={200: "Success", 500: "Internal Error"}
    )
    def post(self, request: Request) -> JsonResponse:
        try:
            user: User = request.user
            data: dict[str, any] = json.loads(request.FILES["data"].read().decode("utf-8"))
            thread = threading.Thread(target=self.data_cleaner, args=(user, data))
            thread.start()
            return JsonResponse({"success": "file received"}, status=200)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=500)

    """ Parses the .json data and formates them, then saves the pre and post processed data through class ScrapingResult, creates an instance of Neo4j_Proxyma_Corpus named by the incoming data title. At the end, notifies the requesting user when data processing is done.
    :params user: class User's instance, used to associate the requesting user to the Neo4j_Proxyma_Corpus instance
    :params data: scraped data, .json format
    :returns: None
    """
    def data_cleaner(self, user: User, data: dict[str, any]) -> None:
        processed_corpus = copy.deepcopy(data)
        for elem in processed_corpus:
            for index, activity in enumerate(elem["activities"]):
                dataROMEO = self.askROMEO(activity["activity"], index)

                personnal_resources = []
                if dataROMEO is not None:
                    for part in dataROMEO[0]["competencesRome"]:
                        personnal_resources.append({
                            "description": part["libelleCompetence"],
                            "type": "k" if part["typeCompetence"] == "SAVOIR" else "s" if part["typeCompetence"] == "COMPETENCE-DETAILLEE" else "a",
                        })

                activity["personnal_resources"] = [personnal_resource for personnal_resource in personnal_resources]
                activity["objectives"] = []
                activity['index'] = index

                for i, task in enumerate(activity["tasks"]):
                    task["index"] = i
                    task["validated"] = False
                    task["date"] = define_date(task["date"])

                    parsedTask = define_objective(task["task"])
                    task["objective"] = parsedTask["objective"]
                    actions = parsedTask["action"]
                    
                    task["actions"].append(parsedTask["action"])
					
                    parsedDetails = details_parser(task["details"])
                    task["environmental_resources"] = [{"description": environmental_resource, "type": None} for environmental_resource in parsedDetails["resources"]]
                    objectives = parsedDetails["objectives"]
                    if len(objectives) > 0:
                        for objective in objectives:
                            activity["objectives"].append(objective)
                    if task["objective"] is not None:
                        activity["objectives"].append(task["objective"])

                    task["personnal_resources"] = []
                    for index, action in enumerate(task["actions"]):
                        dataROMEO = self.askROMEO(action, index)
                        if dataROMEO is not None:
                            for part in dataROMEO[0]["competencesRome"]:
                                task["personnal_resources"].append({
                                    "description": part["libelleCompetence"],
                                    "type": "k" if part["typeCompetence"] == "SAVOIR" else "s" if part["typeCompetence"] == "COMPETENCE-DETAILLEE" else "a",
                                })

                    del task["objective"]
                    del task["task"]
                    del task["details"]

        ScrapingResult(mapTitle=data[0]["corpus"], initial_corpus=data, processed_corpus=processed_corpus, user=user).save()
        scrap_res = ScrapingResult.objects.get(mapTitle=data[0]["corpus"], user=user)
        Neo4j_Proxyma_Corpus.get_or_create({
            "uid": scrap_res.uid,
            "title": data[0]["corpus"],
            "user": user.id,
            "validated": False
        })
        user_notify(request=self.request, user=user, message=f"DATA_<{data[0]['corpus']}>", subject="Collecte terminée", template="data_register_confirmation_email.html")
        
    """ Consumes romeo API in order to compare tasks name with rome cards and try to retrieve some personnal resources
    :params task: a task's name passed to API
    :params id: the task's index, used by the API as an id
    :returns: 2 results in an array if the request runs fine, else None
    """
    def askROMEO(self, task: str, id: int) -> None|list[dict]:
        api_url = "https://entreprise.francetravail.fr/connexion/oauth2/access_token"
        headers = { "Content-Type": "application/x-www-form-urlencoded" }
        params = { "realm": "/partenaire" }
        data = {
            "grant_type": "client_credentials",
            "client_id": "PAR_catcap_c23c84ae520bf3b691fff90a5a638996357ff8cae26bfaef07db931ad0ab6f46",
            "client_secret": "ff6e167366ffb4ff4f37ba14cad92e003876e70ab3ba6f583eae4fcb4eacf7e8",
            "scope": "api_romeov1"
        }
        try:
            response = requests.post(api_url, headers=headers, params=params, data=data)
            if response.status_code == 200:
                data = response.json()
                self.access_token = data["access_token"]
                api_url = "https://api.francetravail.io/partenaire/romeo/v1/predictionCompetences"
                headers = { 
                    "Content-Type": "application/json",
                    "Authorization": f"Bearer {self.access_token}",
                    "nomappelant": "CaTCaP",
                    "seuilScorePrediction": 0.8
                }
                data = [{
                    "intitule": f"{task}",
                    "identifiant": f"{id}"
                }]
                response = requests.post(api_url, headers=headers, params=params, json=data)
                if response.status_code == 200:
                    data = response.json()
                    if data[0]["exceptionsRomeo"] is not None:
                        return None
                    else:
                        return data
        except:
            return None


class CorpusNames(APIView):
    my_tags = ["Scraping"]
    """ Gets the user's corpus names and ids
    :params request.user: class User's instance, the requesting user
    :returns: HTTP_Response 200 with an array within all corpus name and id (each inside separate dict) if request runs fine, else 500
    """
    
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        operation_summary="Get all corpus names",
        responses={200: "Success", 500: "Internal Error"}
    )
    def get(self, request: Request) -> JsonResponse:
        try:
            user = request.user
            response = []
            for corpus in user.ScrapResults.all():
                obj = {
                    "id": corpus.uid,
                    "title": corpus.mapTitle,
                }
                response.append(obj)
            return JsonResponse(response, safe=False, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class CorpusHandler(APIView):
    my_tags = ["Scraping"]
    """ Gets one entire corpus
    :params corpus_id: an unique identifier used to retrieves the requested corpus
    :returns: HTTP_Response 200 with the requested corpus title, its id and its processed data if request runs fine, else 500
    """
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        operation_summary="Get one corpus",
        responses={200: "Success", 500: "Internal Error"}
    )
    def get(self, request: Request, corpus_id: str) -> JsonResponse:
        try:
            corpus = request.user.ScrapResults.get(uid=corpus_id)
            return JsonResponse({"corpus": corpus.mapTitle, "id": corpus.uid, "data": corpus.processed_corpus[0]}, safe=False, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"error": str(e)}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class ValidateActivity(APIView):
    my_tags = ["Scraping"]
    """ Gets existing map in order to offer to requesting user a preview of his work
    :params corpus_title: used to retrieves the good Neo4j_Proxyma_Corpus
    :returns: HTTP_Response 200 with map's data if request runs fine, else 500
    """
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        operation_summary="Get map",
        responses={200: "Success", 500: "Internal Error"}
    )
    def get(self, request: Request, corpus_title: str, corpus_id: str) -> JsonResponse:
        try:
            proxyma: Neo4j_Proxyma_Corpus
            try:
                proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=corpus_id, user=request.user.id)
            except:
                proxyma = Neo4j_Proxyma_Corpus(uid=corpus_id, title=corpus_title, user=request.user.id).save()
            data = []
            data.append(proxyma.to_json())
            
            for action in proxyma.action.all():
                node = action.to_json()
                if not any(existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)


            for activity in proxyma.activity.all():
                node = activity.to_json()
                if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)


            for actor in proxyma.actor.all():
                node = actor.to_json()
                if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)


            for environmental_resource in proxyma.environmental_resource.all():
                node = environmental_resource.to_json()
                if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)


            for objective in proxyma.objective.all():
                node = objective.to_json()
                if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)


            for performance in proxyma.performance.all():
                node = performance.to_json()
                if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)


            for personnal_resource in proxyma.personnal_resource.all():
                node = personnal_resource.to_json()
                if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)


            for scheme in proxyma.scheme.all():
                node = scheme.to_json()
                if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)


            for situation in proxyma.situation.all():
                node = situation.to_json()
                if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
                    data.append(node)

            return JsonResponse({"proxyma": proxyma.title, "data": data}, safe=False, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"error": str(e)}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    """ Deletes a corpus both in SQL and neo4j
    :params corpus_id: the SQL corpus' id
    :params request.user: class User's instance, the requesting user, used to find the good Neo4j_Proxyma_Corpus
    :returns: HTTP_Response 200 if corpus is deleted, else 500
    """
    class ValidateActivitySerializer(serializers.Serializer):
        file = serializers.FileField()
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        operation_summary="Delete corpus",
        responses={200: "Success", 500: "Internal Error"}
    )
    def delete(self, request: Request, corpus_id: str) -> JsonResponse:
        try:
            corpus = request.user.ScrapResults.get(uid=corpus_id)
            proxyma = Neo4j_Proxyma_Corpus.nodes.filter(uid=corpus_id, validated=False)
            if len(proxyma):
                proxyma[0].delete()
            corpus.delete()
            return JsonResponse({"success": "corpus deleted"}, status=status.HTTP_200_OK)
        except:
            return JsonResponse({"error": "something goes wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    """ Validates a task through an async thread
    :params request.user: class User's instance, the requesting user
    :params request.file: a .json file wich contains the task converted data to register in neo4j
    :params corpus_id: the ScrapingResult unique id used to retrieves the proper data
    :params activityIndex: index of ScrapingResult activity to update
    :params taskIndex: index of activity task to update
    :returns: HTTP_Response 200 if thread runs fine, else 500
    """
    class ValidateActivitySerializer(serializers.Serializer):
        file = serializers.FileField()
        
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        operation_summary="Validate activity",
        request_body=ValidateActivitySerializer,
        responses={200: "Success", 500: "Internal Error"}
    )
    def post(self, request: Request, corpus_id: str, activityIndex: str, taskIndex: str) -> JsonResponse:
        try:
            work_corpus = json.load(request.FILES["work_corpus"].file)
            corpus = request.user.ScrapResults.get(uid=corpus_id)
            corpus.validate_task(activityIndex, taskIndex, work_corpus)
            file = request.FILES["file"]
            proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=corpus_id)
            thread = threading.Thread(target=self.update_corpus_async, args=(request.user, proxyma, file))
            thread.start()
            return JsonResponse({"success": "activity updated"}, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    """ Parses the .json file to retrieves elements to store in neo4j and then notifies the requesting user when achieved
    :params user: class User's instance, the requesting user, used to associate the Neo4j_Proxyma_Corpus instance to the user's last active group
    :params proxyma: class Neo4j_Proxyma_Corpus' instance, corpus to update
    :params file: .json format file, contains the data
    :returns: None
    """
    def update_corpus_async(self, user: User, proxyma: Neo4j_Proxyma_Corpus, file: dict[str, any]) -> None:
        if file.name.endswith(".json"):
            try:
                data = json.load(file.file)
                if len(data) > 0:
                    title = proxyma.title
                    
                    remainNodes = []
                    nodesToCheck = [proxyma.uid]
                    for action in proxyma.action.all():
                        nodesToCheck.append(action.uid)
                    for activity in proxyma.activity.all():
                        nodesToCheck.append(activity.uid)
                    for actor in proxyma.actor.all():
                        nodesToCheck.append(actor.uid)
                    for environmental_resource in proxyma.environmental_resource.all():
                        nodesToCheck.append(environmental_resource.uid)
                    for objective in proxyma.objective.all():
                        nodesToCheck.append(objective.uid)
                    for performance in proxyma.performance.all():
                        nodesToCheck.append(performance.uid)
                    for personnal_resource in proxyma.personnal_resource.all():
                        nodesToCheck.append(personnal_resource.uid)
                    for scheme in proxyma.scheme.all():
                        nodesToCheck.append(scheme.uid)
                    for situation in proxyma.situation.all():
                        nodesToCheck.append(situation.uid)
                    for elem in data:
                        remainNodes.append(elem["properties"]["uid"])
                    
                    newNodes = [node for node in list(set(remainNodes) - set(nodesToCheck))]
                    nodesToDelete = [node for node in list(set(nodesToCheck) - set(remainNodes))]
                    copyRemainNodes = copy.deepcopy(remainNodes)
                    for node in copyRemainNodes:
                        if node in newNodes:
                            remainNodes.remove(node)
                            
                    for elem in data:
                        labels = elem["labels"][0]
                        properties = elem["properties"]
                        if labels != "Proxyma_Corpus":
                            date_str = properties["created_at"]
                            date = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()
                        if properties["uid"] in newNodes:
                            match labels:
                                case "Action":
                                    Neo4j_Action(uid=properties["uid"], description=properties["description"], created_at=date).save()
                                case "Activity":
                                    Neo4j_Activity(uid=properties["uid"], description=properties["description"], created_at=date).save()
                                case "Actor":
                                    Neo4j_Actor(uid=properties["uid"], name=properties["name"], created_at=date).save()
                                case "Environmental_Resource":
                                    Neo4j_Environmental_Resource(uid=properties["uid"], type=properties["type"], description=properties["description"], created_at=date).save()
                                case "Objective":
                                    Neo4j_Objective(uid=properties["uid"], description=properties["description"], created_at=date).save()
                                case "Performance":
                                    Neo4j_Performance(uid=properties["uid"], criterion=properties["criterion"], value=properties["value"], unit=properties["unit"], created_at=date).save()
                                case "Personnal_Resource":
                                    Neo4j_Personnal_Resource(uid=properties["uid"], type=properties["type"], description=properties["description"], created_at=date).save()
                                case "Scheme":
                                    Neo4j_Scheme(uid=properties["uid"], actions_sequence=properties["actions_sequence"], created_at=date).save()
                                case "Situation":
                                    Neo4j_Situation(uid=properties["uid"], description=properties["description"], created_at=date).save()

                        elif properties["uid"] in remainNodes:
                            match labels:
                                case "Proxyma_Corpus":
                                    if proxyma.title != properties["title"]:
                                        proxyma.title = properties["title"]
                                        proxyma.save()
                                case "Action":
                                    action = Neo4j_Action.nodes.get(uid=properties["uid"])
                                    if action.description != properties["description"]:
                                        action.description = properties["description"]
                                        action.save()
                                case "Activity":
                                    activity = Neo4j_Activity.nodes.get(uid=properties["uid"])
                                    if activity.description != properties["description"]:
                                        activity.description = properties["description"]
                                        activity.save()
                                case "Actor":
                                    actor = Neo4j_Actor.nodes.get(uid=properties["uid"])
                                    if actor.name != properties["name"]:
                                        actor.name = properties["name"]
                                        actor.save()
                                case "Environmental_Resource":
                                    environmental_resource = Neo4j_Environmental_Resource.nodes.get(uid=properties["uid"])
                                    for key, value in properties.items():
                                        if key != "uid" and key != "created_at" and getattr(environmental_resource, key) != value:
                                            setattr(environmental_resource, key, value)
                                    environmental_resource.save()
                                case "Objective":
                                    objective = Neo4j_Objective.nodes.get(uid=properties["uid"])
                                    if objective.description != properties["description"]:
                                        objective.description = properties["description"]
                                        objective.save()
                                case "Performance":
                                    performance = Neo4j_Performance.nodes.get(uid=properties["uid"])
                                    for key, value in properties.items():
                                        if key != "uid" and key != "created_at" and getattr(performance, key) != value:
                                            setattr(performance, key, value)
                                    performance.save()
                                case "Personnal_Resource":
                                    personnal_resource = Neo4j_Personnal_Resource.nodes.get(uid=properties["uid"])
                                    for key, value in properties.items():
                                        if key != "uid" and key != "created_at" and getattr(personnal_resource, key) != value:
                                            setattr(personnal_resource, key, value)
                                    personnal_resource.save()
                                case "Scheme":
                                    scheme = Neo4j_Scheme.nodes.get(uid=properties["uid"])
                                    if scheme.actions_sequence != properties["actions_sequence"]:
                                        scheme.actions_sequence = properties["actions_sequence"]
                                        scheme.save()
                                case "Situation":
                                    situation = Neo4j_Situation.nodes.get(uid=properties["uid"])
                                    if situation.description != properties["description"]:
                                        situation.description = properties["description"]
                                        situation.save()                


                    for elem in data:
                        labels = elem["labels"][0]
                        properties = elem["properties"]
                        relationships = elem["relationships"]
                        if relationships is not None:
                            match labels:
                                case "Proxyma_Corpus":

                                    db_actions = set([action.uid for action in proxyma.action.all()])
                                    incoming_actions = set(relationships["actions"])
                                    disconnect_actions = [proxyma.action.disconnect(Neo4j_Action.nodes.get(uid=action)) for action in list(db_actions - incoming_actions)]
                                    connect_actions = [proxyma.action.connect(Neo4j_Action.nodes.get(uid=action)) for action in list(incoming_actions - db_actions)]

                                    db_activities = set([activity.uid for activity in proxyma.activity.all()])
                                    incoming_activities = set(relationships["activities"])
                                    disconnect_activities = [proxyma.activity.disconnect(Neo4j_Activity.nodes.get(uid=activity)) for activity in list(db_activities - incoming_activities)]
                                    connect_activities = [proxyma.activity.connect(Neo4j_Activity.nodes.get(uid=activity)) for activity in list(incoming_activities - db_activities)]

                                    db_actors = set([actor.uid for actor in proxyma.actor.all()])
                                    incoming_actors = set(relationships["actors"])
                                    disconnect_actors = [proxyma.actor.disconnect(Neo4j_Actor.nodes.get(uid=actor)) for actor in list(db_actors - incoming_actors)]
                                    connect_actors = [proxyma.actor.connect(Neo4j_Actor.nodes.get(uid=actor)) for actor in list(incoming_actors - db_actors)]

                                    db_environmental_resources = set([environmental_resource.uid for environmental_resource in proxyma.environmental_resource.all()])
                                    incoming_environmental_resource = set(relationships["environmental_resources"])
                                    disconnect_environmental_resource = [proxyma.environmental_resource.disconnect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource)) for environmental_resource in list(db_environmental_resources - incoming_environmental_resource)]
                                    connect_environmental_resource = [proxyma.environmental_resource.connect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource)) for environmental_resource in list(incoming_environmental_resource - db_environmental_resources)]

                                    db_objectives = set([objective.uid for objective in proxyma.objective.all()])
                                    incoming_objectives = set(relationships["objectives"])
                                    disconnect_objectives = [proxyma.objective.disconnect(Neo4j_Objective.nodes.get(uid=objective)) for objective in list(db_objectives - incoming_objectives)]
                                    connect_objectives = [proxyma.objective.connect(Neo4j_Objective.nodes.get(uid=objective)) for objective in list(incoming_objectives - db_objectives)]

                                    db_performances = set([performance.uid for performance in proxyma.performance.all()])
                                    incoming_performances = set(relationships["performances"])
                                    disconnect_performances = [proxyma.performance.disconnect(Neo4j_Performance.nodes.get(uid=performance)) for performance in list(db_performances - incoming_performances)]
                                    connect_performances = [proxyma.performance.connect(Neo4j_Performance.nodes.get(uid=performance)) for performance in list(incoming_performances - db_performances)]

                                    db_personnal_resources = set([personnal_resource.uid for personnal_resource in proxyma.personnal_resource.all()])
                                    incoming_personnal_resources = set(relationships["personnal_resources"])
                                    disconnect_personnal_resources = [proxyma.personnal_resource.disconnect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource)) for personnal_resource in list(db_personnal_resources - incoming_personnal_resources)]
                                    connect_personnal_resources = [proxyma.personnal_resource.connect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource)) for personnal_resource in list(incoming_personnal_resources - db_personnal_resources)]

                                    db_schemes = set([scheme.uid for scheme in proxyma.scheme.all()])
                                    incoming_schemes = set(relationships["schemes"])
                                    disconnect_schemes = [proxyma.scheme.disconnect(Neo4j_Scheme.nodes.get(uid=scheme)) for scheme in list(db_schemes - incoming_schemes)]
                                    connect_schemes = [proxyma.scheme.connect(Neo4j_Scheme.nodes.get(uid=scheme)) for scheme in list(incoming_schemes - db_schemes)]

                                    db_situations = set([situation.uid for situation in proxyma.situation.all()])
                                    incoming_situations = set(relationships["situations"])
                                    disconnect_situations = [proxyma.situation.disconnect(Neo4j_Situation.nodes.get(uid=situation)) for situation in list(db_situations - incoming_situations)]
                                    connect_situations = [proxyma.situation.connect(Neo4j_Situation.nodes.get(uid=situation)) for situation in list(incoming_situations - db_situations)]

                                case "Activity":
                                    activity = Neo4j_Activity.nodes.get(uid=properties["uid"])

                                    db_objectives = set([objective.uid for objective in activity.objective.all()])
                                    incoming_objectives = set(relationships["objectives"])
                                    disconnect_objectives = [activity.objective.disconnect(Neo4j_Objective.nodes.get(uid=objective)) for objective in list(db_objectives - incoming_objectives)]
                                    connect_objectives = [activity.objective.connect(Neo4j_Objective.nodes.get(uid=objective)) for objective in list(incoming_objectives - db_objectives)]

                                    db_schemes = set([scheme.uid for scheme in activity.scheme.all()])
                                    incoming_schemes = set(relationships["schemes"])
                                    disconnect_schemes = [activity.scheme.disconnect(Neo4j_Scheme.nodes.get(uid=scheme)) for scheme in list(db_schemes - incoming_schemes)]
                                    connect_schemes = [activity.scheme.connect(Neo4j_Scheme.nodes.get(uid=scheme)) for scheme in list(incoming_schemes - db_schemes)]

                                    db_situations = set([situation.uid for situation in activity.situation.all()])
                                    incoming_situations = set(relationships["situations"])
                                    disconnect_situations = [activity.situation.disconnect(Neo4j_Situation.nodes.get(uid=situation)) for situation in list(db_situations - incoming_situations)]
                                    connect_situations = [activity.situation.connect(Neo4j_Situation.nodes.get(uid=situation)) for situation in list(incoming_situations - db_situations)]

                                case "Actor":
                                    actor = Neo4j_Actor.nodes.get(uid=properties["uid"])

                                    db_personnal_resources = set([personnal_resource.uid for personnal_resource in actor.personnal_resource.all()])
                                    incoming_personnal_resources = set(relationships["personnal_resources"])
                                    disconnect_personnal_resources = [actor.personnal_resource.disconnect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource)) for personnal_resource in list(db_personnal_resources - incoming_personnal_resources)]
                                    connect_personnal_resources = [actor.personnal_resource.connect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource)) for personnal_resource in list(incoming_personnal_resources - db_personnal_resources)]

                                case "Objective":
                                    objective = Neo4j_Objective.nodes.get(uid=properties["uid"])
                                    
                                    db_performances = set([performance.uid for performance in objective.performance.all()])
                                    incoming_performances = set(relationships["performances"])
                                    disconnect_performances = [objective.performance.disconnect(Neo4j_Performance.nodes.get(uid=performance)) for performance in list(db_performances - incoming_performances)]
                                    connect_performances = [objective.performance.connect(Neo4j_Performance.nodes.get(uid=performance)) for performance in list(incoming_performances - db_performances)]

                                case "Scheme":
                                    scheme = Neo4j_Scheme.nodes.get(uid=properties["uid"])
                                    
                                    db_actions = set([action.uid for action in scheme.action.all()])
                                    incoming_actions = set(relationships["actions"])
                                    disconnect_actions = [scheme.action.disconnect(Neo4j_Action.nodes.get(uid=action)) for action in list(db_actions - incoming_actions)]
                                    connect_actions = [scheme.action.connect(Neo4j_Action.nodes.get(uid=action)) for action in list(incoming_actions - db_actions)]

                                    db_actors = set([actor.uid for actor in scheme.actor.all()])
                                    incoming_actors = set(relationships["actors"])
                                    disconnect_actors = [scheme.actor.disconnect(Neo4j_Actor.nodes.get(uid=actor)) for actor in list(db_actors - incoming_actors)]
                                    connect_actors = [scheme.actor.connect(Neo4j_Actor.nodes.get(uid=actor)) for actor in list(incoming_actors - db_actors)]

                                    db_environmental_resources = set([environmental_resource.uid for environmental_resource in scheme.environmental_resource.all()])
                                    incoming_environmental_resource = set(relationships["environmental_resources"])
                                    disconnect_environmental_resource = [scheme.environmental_resource.disconnect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource)) for environmental_resource in list(db_environmental_resources - incoming_environmental_resource)]
                                    connect_environmental_resource = [scheme.environmental_resource.connect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource)) for environmental_resource in list(incoming_environmental_resource - db_environmental_resources)]

                if len(Neo4j_Proxyma_Corpus.nodes.filter(title__startswith=proxyma.title)) > 1:
                    proxyma.title = f"{proxyma.title}_{len(Neo4j_Proxyma_Corpus.nodes.filter(title__startswith=proxyma.title)) +1}"
                proxyma.validated = True
                proxyma.group = user.last_active_group.id
                proxyma.save()
                user_notify(request=self.request, user=user, message=f"OK_<{title}>", subject="Enregistrement des données terminé", template="data_register_confirmation_email.html")
            except Exception as e:
                return JsonResponse({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class GetRawData(APIView):
    my_tags = ["Scraping"]
    """ Gets pre and post process data for a single ScrapingResult
    :params request.user: class User's instance, requesting user
    :params corpus_id: ScrapingResult instance's id
    :returns: HTTP_Response 200 with pre and post process data if exists, else 500
    """
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        operation_summary="Get raw data",
        responses={200: "Success", 500: "Internal Error"}
    )
    def get(self, request: Request, corpus_id: str) -> JsonResponse:
        try:
            corpus = request.user.ScrapResults.get(uid=corpus_id)
            return JsonResponse({"pre_process_data": corpus.initial_corpus, "post_process_data": corpus.processed_corpus})
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
