from django.apps import AppConfig


class ScrapingHandlerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'scraping_handler'
