# Generated by Django 4.2 on 2024-01-30 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraping_handler', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scrapingresult',
            name='processed_corpus',
            field=models.JSONField(null=True),
        ),
    ]
