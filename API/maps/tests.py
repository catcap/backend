from authentication.models import User, Group
from Neo4j.models import Neo4j_Proxyma_Corpus
from django.test import TestCase
from django.urls import reverse
import json

class ProxymaCorpusListTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		self.group = Group(public_group=False, owner=self.user)
		self.group.save()
		self.user.groups.add(self.group.id)
		self.user.last_active_group = self.group
		self.user.save()

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.get(reverse("maps:proxyma_list"))
		self.assertEqual(response.status_code, 200)
		corpus = response.json()
		self.assertEqual(type(corpus), list)

	def test_get_401(self):
		response = self.client.get(reverse("maps:proxyma_list"))
		self.assertEqual(response.status_code, 401)

class ProxymaCorpusTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		self.group = Group(public_group=False, owner=self.user)
		self.group.save()
		self.user.groups.add(self.group.id)
		self.user.last_active_group = self.group
		self.user.save()

	def test_get_200(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		proxyma = Neo4j_Proxyma_Corpus(title="test").save()
		response = self.client.get(reverse("maps:select_proxyma", kwargs={"corpus_id": proxyma.uid}))
		corpus_title = response.json()["proxyma"]
		data = response.json()["data"]
		self.assertEqual(response.status_code, 200)
		self.assertEqual(type(corpus_title), str)
		self.assertEqual(corpus_title, proxyma.title)
		self.assertEqual(type(data), list)
		proxyma.delete()

	def test_get_500(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.get(reverse("maps:select_proxyma", kwargs={"corpus_id": "1234"}))
		self.assertEqual(response.status_code, 500)

	def test_get_401(self):
		proxyma = Neo4j_Proxyma_Corpus(title="test").save()
		response = self.client.get(reverse("maps:select_proxyma", kwargs={"corpus_id": proxyma.uid}))
		self.assertEqual(response.status_code, 401)
		proxyma.delete()

	def test_put_204(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		proxyma = Neo4j_Proxyma_Corpus(title="test").save()
		response = self.client.put(reverse("maps:select_proxyma", kwargs={"corpus_id": proxyma.uid}), {"corpus_title": "pouet"}, content_type="application/json")
		proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=proxyma.uid)
		self.assertEqual(response.status_code, 204)
		self.assertEqual(proxyma.title, "pouet")
		proxyma.delete()

	def test_put_500(self):
		response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
		response = self.client.put(reverse("maps:select_proxyma", kwargs={"corpus_id": "proxyma.uid"}), {"corpus_title": "pouet"}, content_type="application/json")
		self.assertEqual(response.status_code, 500)
		response = self.client.put(reverse("maps:select_proxyma", kwargs={"corpus_id": "proxyma.uid"}), {"corpus_title": "pouet"})
		self.assertEqual(response.status_code, 500)
		response = self.client.put(reverse("maps:select_proxyma", kwargs={"corpus_id": "proxyma.uid"}))
		self.assertEqual(response.status_code, 500)