from django.urls import path
from maps.views import *

app_name = 'maps'

urlpatterns = [
	# PROXYMA_CORPUS routes
	path('proxyma_list', ProxymaCorpusList.as_view(), name='proxyma_list'),
	path('select_proxyma/<str:corpus_id>', ProxymaCorpus.as_view(), name='select_proxyma'),


]