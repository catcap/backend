from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status


from Neo4j.models.neo4j_Proxyma_Corpus import Neo4j_Proxyma_Corpus

from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers

class ProxymaCorpusList(APIView):
	my_tags = ["Proxyma"]
	permission_classes = [IsAuthenticated]
 
	@swagger_auto_schema(
		operation_summary="Get all proxyma corpus",
		responses={200: "Success", 500: "Internal Error"}
	)
	@csrf_exempt
	def get(self, request):
		try:
			user_group = request.user.last_active_group
			response = []
			for corpus in Neo4j_Proxyma_Corpus.nodes.filter(group=user_group.id).order_by("title"):
				if not corpus.delete_mark and corpus.validated:
					obj = {
						"id": corpus.uid,
						"title": corpus.title,
						"corpus_timestamp": corpus.corpus_timestamp,
					}
					response.append(obj)
			return JsonResponse(response, safe=False, status=status.HTTP_200_OK)
		except Exception as e:
			return JsonResponse({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

	

class ProxymaCorpus(APIView):
	my_tags = ["Proxyma"]
	permission_classes = [IsAuthenticated]
 
	@swagger_auto_schema(
		operation_summary="Get proxyma corpus",
		responses={200: "Success", 500: "Internal Error"}
	)
	@csrf_exempt
	def get(self, request, corpus_id):
		try:
			proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=corpus_id)
			data = []
			data.append(proxyma.to_json())
			
			for action in proxyma.action.all():
				node = action.to_json()
				if not any(existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			for activity in proxyma.activity.all():
				node = activity.to_json()
				if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			for actor in proxyma.actor.all():
				node = actor.to_json()
				if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			for environmental_resource in proxyma.environmental_resource.all():
				node = environmental_resource.to_json()
				if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			for objective in proxyma.objective.all():
				node = objective.to_json()
				if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			for performance in proxyma.performance.all():
				node = performance.to_json()
				if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			for personnal_resource in proxyma.personnal_resource.all():
				node = personnal_resource.to_json()
				if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			for scheme in proxyma.scheme.all():
				node = scheme.to_json()
				if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			for situation in proxyma.situation.all():
				node = situation.to_json()
				if not any (existing_data["properties"]["uid"] == node["properties"]["uid"] for existing_data in data):
					data.append(node)


			return JsonResponse({"proxyma": proxyma.title, "data": data}, safe=False, status=status.HTTP_200_OK)
		except Exception as e:
			return JsonResponse({"error": str(e)}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

	class ProxymaSerializer(serializers.Serializer):
		corpus_title = serializers.CharField(required=False)
  
	permission_classes = [IsAuthenticated]
	@swagger_auto_schema(
		operation_summary="Delete proxyma corpus",
		request_body=ProxymaSerializer,
		responses={204: "Success", 500: "Internal Error"}
	)
	def put(self, request, corpus_id):
		try:
			corpus = Neo4j_Proxyma_Corpus.nodes.get(uid=corpus_id)
			if request.data.get("corpus_title") is not None:
				corpus.title = request.data.get("corpus_title")
				corpus.save()
				return JsonResponse({"success": "corpus_updated"}, status=status.HTTP_204_NO_CONTENT)
		except:
			return JsonResponse({"error": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
