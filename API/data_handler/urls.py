from django.urls import path
from data_handler.views import *

app_name = "data_handler"

urlpatterns = [
	path('handle_data', DataView.as_view(), name='post_data'),
	path('handle_data/<str:corpus_id>', DataView.as_view(), name='delete/put_data'),
]