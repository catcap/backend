from authentication.models import User
from Neo4j.models import Neo4j_Proxyma_Corpus
from django.test import TestCase
from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile
from unittest.mock import patch
import json

class DataViewTest(TestCase):
	def setUp(self):
		self.user = User.objects.create_user(email="jondoe@mail.com", first_name="jon", last_name="doe", password="123456", is_active=True)
		data = [{}]
		self.json_data = json.dumps(data)

	def test_post_200(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			response = self.client.post(reverse("data_handler:post_data"), {"file": self.json_data})
			self.assertEqual(response.status_code, 200)

	def test_post_500(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			response = self.client.post(reverse("data_handler:post_data"))
			self.assertEqual(response.status_code, 500)

	def test_post_401(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("data_handler:post_data"), {"file": self.json_data})
			self.assertEqual(response.status_code, 401)

	def test_delete_200(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			proxyma = Neo4j_Proxyma_Corpus(title="test").save()
			response = self.client.delete(reverse("data_handler:delete/put_data", kwargs={"corpus_id": proxyma.uid}))
			self.assertEqual(response.status_code, 200)
			proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=proxyma.uid)
			self.assertEqual(proxyma.delete_mark, True)
			proxyma.delete()

	def test_delete_500(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			response = self.client.delete(reverse("data_handler:delete/put_data", kwargs={"corpus_id": "1234"}))
			self.assertEqual(response.status_code, 500)

	def test_delete_401(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			proxyma = Neo4j_Proxyma_Corpus(title="test").save()
			response = self.client.delete(reverse("data_handler:delete/put_data", kwargs={"corpus_id": proxyma.uid}))
			self.assertEqual(response.status_code, 401)
			proxyma.delete()

	def test_put_200(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			proxyma = Neo4j_Proxyma_Corpus(title="test").save()
			response = self.client.put(reverse("data_handler:delete/put_data", kwargs={"corpus_id": proxyma.uid}), {"file": self.json_data}, content_type="application/json")
			self.assertEqual(response.status_code, 202)
			proxyma.delete()

	def test_put_500(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			response = self.client.post(reverse("login"), { "email": self.user.email, "password": "123456" })
			response = self.client.put(reverse("data_handler:delete/put_data", kwargs={"corpus_id": "1234"}), {"file": self.json_data}, content_type="application/json")
			self.assertEqual(response.status_code, 500)
			proxyma = Neo4j_Proxyma_Corpus(title="test").save()
			response = self.client.put(reverse("data_handler:delete/put_data", kwargs={"corpus_id": proxyma.uid}), content_type="application/json")
			self.assertEqual(response.status_code, 500)
			proxyma.delete()

	def test_put_401(self):
		with patch("data_handler.views.threading.Thread.start", return_value=None):
			proxyma = Neo4j_Proxyma_Corpus(title="test").save()
			response = self.client.put(reverse("data_handler:delete/put_data", kwargs={"corpus_id": proxyma.uid}), {"file": self.json_data})
			self.assertEqual(response.status_code, 401)
			proxyma.delete()
