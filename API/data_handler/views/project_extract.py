import calendar, csv, datetime, json, locale, re, threading, uuid

locale.setlocale(locale.LC_ALL, '')

from io import StringIO

from django.http import JsonResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.request import Request
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from neomodel import db, DoesNotExist
from Neo4j.models import *
from WS_notifs.user_notify import user_notify

from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers



class DataView(APIView):
    """ Retrieves a map in .json format and runs a thread wich will save the map in Neo4j
    :params request.file: the .json file who contains the map's data
    :returns: HTTP_Response 200 if the thread starts, else 500
    """
    my_tags = ["Data"]
    permission_classes = [IsAuthenticated]
    class DataSerializer(serializers.Serializer):
        file = serializers.FileField()
        
    @swagger_auto_schema(
        request_body=DataSerializer,
        operation_id='data_import',
        operation_description='Imports a map in Neo4j',
        responses={200: 'data_import_started', 500: 'error occurred'}
    )
    
    @csrf_exempt
    def post(self, request: Request) -> JsonResponse:
        try:
            file: dict[str, any] = request.data.get("file")
            if file is None:
                raise Exception
            user_group = request.user.last_active_group
            thread = threading.Thread(target=self.extract_data_from_json, args=(file, user_group))
            thread.start()
            return JsonResponse({'message': 'data_import_started'}, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    """ Reads, extracts and saves the map in Neo4j, notifies the requesting user with WebSocket when done
    :params data: the .json file who contains the map's data
    :params user_group: the requesting user's group's id
    :returns: None
    """
    def extract_data_from_json(self, data: dict[str, any], user_group: str) -> None:
        # Open the JSON file
        title = ""
        if len(data) > 0:
            for elem in data:
                labels = elem["labels"][0]
                properties = elem["properties"]
                # creates all nodes in Neo4j (or retrieves them if they exist)
                match labels:
                    case "Proxyma_Corpus":
                        Neo4j_Proxyma_Corpus.get_or_create({
                            "uid": properties["uid"],
                            "title": properties["title"],
                        })
                        title = properties["title"]
                    case "Action":
                        action = Neo4j_Action.get_or_create({
                            "uid": properties["uid"],
                            "description": properties["description"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            action.created_at = date_obj
                            action.save()
                    case "Activity":
                        activity = Neo4j_Activity.get_or_create({
                            "uid": properties["uid"],
                            "description": properties["description"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            activity.created_at = date_obj
                            activity.save()
                    case "Actor":
                        actor = Neo4j_Actor.get_or_create({
                            "uid": properties["uid"],
                            "name": properties["name"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            actor.created_at = date_obj
                            actor.save()
                    case "Environmental_Resource":
                        environmental_resource = Neo4j_Environmental_Resource.get_or_create({
                            "uid": properties["uid"],
                            "type": properties["type"],
                            "description": properties["description"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            environmental_resource.created_at = date_obj
                            environmental_resource.save()
                    case "Objective":
                        objective = Neo4j_Objective.get_or_create({
                            "uid": properties["uid"],
                            "description": properties["description"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            objective.created_at = date_obj
                            objective.save()
                    case "Performance":
                        performance = Neo4j_Performance.get_or_create({
                            "uid": properties["uid"],
                            "criterion": properties["criterion"],
                            "value": properties["value"],
                            "unit": properties["unit"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            performance.created_at = date_obj
                            performance.save()
                    case "Personnal_Resource":
                        personnal_resource = Neo4j_Personnal_Resource.get_or_create({
                            "uid": properties["uid"],
                            "type": properties["type"],
                            "description": properties["description"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            personnal_resource.created_at = date_obj
                            personnal_resource.save()
                    case "Scheme":
                        scheme = Neo4j_Scheme.get_or_create({
                            "uid": properties["uid"],
                            "actions_sequence": properties["actions_sequence"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            scheme.created_at = date_obj
                            scheme.save()
                    case "Situation":
                        situation = Neo4j_Situation.get_or_create({
                            "uid": properties["uid"],
                            "description": properties["description"],
                            "created_at": datetime.date.today(),
                        })[0]
                        if "created_at" in properties.keys():
                            date_format = "%Y-%m-%d"
                            date_obj = datetime.datetime.strptime(properties["created_at"], date_format)
                            situation.created_at = date_obj
                            situation.save()

            for elem in data:
                labels = elem["labels"][0]
                properties = elem["properties"]
                relationships = elem["relationships"]
                if relationships is not None:
                    # if its labels matches, connects the node with its relationships values
                    match labels:
                        case "Proxyma_Corpus":
                            proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=properties["uid"], title=properties["title"])
                            proxyma.group = user_group.id
                            proxyma.save()
                            for action in relationships["actions"]:
                                try:
                                    proxyma.action.connect(Neo4j_Action.nodes.get(uid=action))
                                    proxyma.save()
                                except:
                                    continue
                            for activity in relationships["activities"]:
                                try:
                                    proxyma.activity.connect(Neo4j_Activity.nodes.get(uid=activity))
                                    proxyma.save()
                                except:
                                    continue
                            for actor in relationships["actors"]:
                                try:
                                    proxyma.actor.connect(Neo4j_Actor.nodes.get(uid=actor))
                                    proxyma.save()
                                except:
                                    continue
                            for environmental_resource in relationships["environmental_resources"]:
                                try:
                                    proxyma.environmental_resource.connect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource))
                                    proxyma.save()
                                except:
                                    continue
                            for objective in relationships["objectives"]:
                                try:
                                    proxyma.objective.connect(Neo4j_Objective.nodes.get(uid=objective))
                                    proxyma.save()
                                except:
                                    continue
                            for performance in relationships["performances"]:
                                try:
                                    proxyma.performance.connect(Neo4j_Performance.nodes.get(uid=performance))
                                    proxyma.save()
                                except:
                                    continue
                            for personnal_resource in relationships["personnal_resources"]:
                                try:
                                    proxyma.personnal_resource.connect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource))
                                    proxyma.save()
                                except:
                                    continue
                            for scheme in relationships["schemes"]:
                                try:
                                    proxyma.scheme.connect(Neo4j_Scheme.nodes.get(uid=scheme))
                                    proxyma.save()
                                except:
                                    continue
                            for situation in relationships["situations"]:
                                try:
                                    proxyma.situation.connect(Neo4j_Situation.nodes.get(uid=situation))
                                    proxyma.save()
                                except:
                                    continue
                        case "Activity":
                            activity = Neo4j_Activity.nodes.get(uid=properties["uid"], description=properties["description"])
                            for objective in relationships["objectives"]:
                                try:
                                    activity.objective.connect(Neo4j_Objective.nodes.get(uid=objective))
                                    activity.save()
                                except:
                                    continue
                            for scheme in relationships["schemes"]:
                                try:
                                    activity.scheme.connect(Neo4j_Scheme.nodes.get(uid=scheme))
                                    activity.save()
                                except:
                                    continue
                            for situation in relationships["situations"]:
                                try:
                                    activity.situation.connect(Neo4j_Situation.nodes.get(uid=situation))
                                    activity.save()
                                except:
                                    continue
                        case "Actor":
                            actor = Neo4j_Actor.nodes.get(uid=properties["uid"], name=properties["name"])
                            for personnal_resource in relationships["personnal_resources"]:
                                try:
                                    actor.personnal_resource.connect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource))
                                    actor.save()
                                except:
                                    continue
                        case "Objective":
                            objective = Neo4j_Objective.nodes.get(uid=properties["uid"], description=properties["description"])
                            for performance in relationships["performances"]:
                                try:
                                    objective.performance.connect(Neo4j_Performance.nodes.get(uid=performance))
                                    objective.save()
                                except:
                                    continue
                        case "Scheme":
                            scheme = Neo4j_Scheme.nodes.get(uid=properties["uid"], actions_sequence=properties["actions_sequence"])
                            for action in relationships["actions"]:
                                try:
                                    scheme.action.connect(Neo4j_Action.nodes.get(uid=action))
                                    scheme.save()
                                except:
                                    continue
                            for actor in relationships["actors"]:
                                try:
                                    scheme.actor.connect(Neo4j_Actor.nodes.get(uid=actor))
                                    scheme.save()
                                except:
                                    continue
                            for environmental_resource in relationships["environmental_resources"]:
                                try:
                                    scheme.environmental_resource.connect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource))
                                    scheme.save()
                                except:
                                    continue

        user_notify(request=self.request, user=self.request.user, message=f"OK_<{title}>", subject="Enregistrement des données terminé", template="data_register_confirmation_email.html")

    """ Marks a Neo4j_Proxyma_Corpus to delete and runs a thread to deletes it and its related nodes if they are not referenced in another map
    :params corpus_id: Neo4j_Proxyma_Corpus's id, used to get the good corpus
    :returns: HTTP_Response 200 if success, else 500
    """
    permission_classes = [IsAuthenticated]
    @swagger_auto_schema(
        operation_id='data_deletion',
        operation_description='Deletes a map in Neo4j',
        responses={200: 'data_deletion_started', 500: 'error occurred'}
    )
    @csrf_exempt
    def delete(self, request: Request, corpus_id: str) -> JsonResponse:
        try:
            proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=corpus_id)
            proxyma.delete_mark = True
            proxyma.save()
            thread = threading.Thread(target=self.delete_corpus_async, args=(corpus_id,))
            thread.start()
            return JsonResponse({'success': 'data_deletion_started'}, status=status.HTTP_200_OK)
        except:
            return JsonResponse({'error': 'error occurred'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    """ Checks if Neo4j_Proxyma_Corpus's related nodes are not referenced in another map, in wich case it deletes them with Neo4j_Proxyma_Corpus, notifies the requesting user with WebSocket
    :params corpus_id: Neo4j_Proxyma_Corpus's id, used to get the good corpus
    :returns: None
    """
    def delete_corpus_async(self, corpus_id: str) -> None:
        nodes_to_delete = []
        proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=corpus_id)
        title = proxyma.title
        nodes_to_delete.append(proxyma)
        # for all Neo4j_Proxyma_Corpus's related nodes, checks if its id is not referenced in any other Neo4j_Proxyma_Corpus
        for activity in proxyma.activity.all():
            if activity not in (corpus.activity.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(activity)
            
        for action in proxyma.action.all():
            if action not in (corpus.action.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(action)

        for objective in proxyma.objective.all():
            if objective not in (corpus.objective.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(objective)
            
        for performance in proxyma.performance.all():
            if performance not in (corpus.performance.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(performance)
            
        for scheme in proxyma.scheme.all():
            if scheme not in (corpus.scheme.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(scheme)

        for actor in proxyma.actor.all():
            if actor not in (corpus.actor.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(actor)

        for personnal_resource in proxyma.personnal_resource.all():
            if personnal_resource not in (corpus.personnal_resource.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(personnal_resource)
                    
        for environmental_resource in proxyma.environmental_resource.all():
            if environmental_resource not in (corpus.environmental_resource.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(environmental_resource)

        for situation in proxyma.situation.all():
            if situation not in (corpus.situation.all() for corpus in Neo4j_Proxyma_Corpus.nodes.filter(uid__ne=proxyma.uid)):
                nodes_to_delete.append(situation)

        for node in nodes_to_delete:
            node.deleted_at = datetime.date.today()
        proxyma.delete()


        user_notify(request=self.request, user=self.request.user, message=f'DEL_<{title}>', subject='Confirmation de suppression de projet', template='data_delete_confirmation_email.html', corpus=proxyma.title)
           
    """ Updates the selected Neo4j_Proxyma_Corpus's related nodes by running an async thread
    :params corpus_id: Neo4j_Proxyma_Corpus's id, used to get the good corpus
    :returns: HTTP_Response 200 if success, else 500
    """
    class DataUpdateSerializer(serializers.Serializer):
        file = serializers.FileField()
        
    @swagger_auto_schema(
        operation_id='data_update',
        operation_description='Updates a map in Neo4j',
        request_body=DataUpdateSerializer,
        responses={202: 'data_update_started', 500: 'error occurred'}
    )
    @csrf_exempt
    def put(self, request: Request, corpus_id: str) -> JsonResponse:
        try:
            file: dict[strn, any] = request.data.get("file")
            if file is None:
                raise Exception
            proxyma = Neo4j_Proxyma_Corpus.nodes.get(uid=corpus_id)
            thread = threading.Thread(target=self.update_corpus_async, args=(proxyma, file))
            thread.start()
            return JsonResponse({'success': 'data_update_started'}, status=status.HTTP_202_ACCEPTED)
        except:
            return JsonResponse({'error': 'error occurred'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    """ Updates the selected Neo4j_Proxyma_Corpus's related nodes by comparing incoming nodes in .json file with existing nodes in Neo4j, removes absentees of .json file, updates remains nodes and creates new nodes, connects or disconnects them, notifies the requesting user when done
    :params corpus_id: Neo4j_Proxyma_Corpus's id, used to get the good corpus
    :params data: the .json file who contains the map's data
    :returns: None
    """
    def update_corpus_async(self, proxyma: Neo4j_Proxyma_Corpus, data: dict[str, any]) -> None:
        if len(data) > 0:
            title = proxyma.title
            corpus_timestamp = proxyma.corpus_timestamp
            remainNodes = []
            nodesToCheck = [proxyma.uid]
            for action in proxyma.action.all():
                
                nodesToCheck.append(action.uid)
            for activity in proxyma.activity.all():
                
                nodesToCheck.append(activity.uid)
            for actor in proxyma.actor.all():
                
                nodesToCheck.append(actor.uid)
            for environmental_resource in proxyma.environmental_resource.all():
                
                nodesToCheck.append(environmental_resource.uid)
            for objective in proxyma.objective.all():
                
                nodesToCheck.append(objective.uid)
            for performance in proxyma.performance.all():
                
                nodesToCheck.append(performance.uid)
            for personnal_resource in proxyma.personnal_resource.all():
                
                nodesToCheck.append(personnal_resource.uid)
            for scheme in proxyma.scheme.all():
                
                nodesToCheck.append(scheme.uid)
            for situation in proxyma.situation.all():
                
                nodesToCheck.append(situation.uid)
            for elem in data:
                remainNodes.append(elem["properties"]["uid"])
            
            newNodes = [node for node in list(set(remainNodes) - set(nodesToCheck))]
            nodesToDelete = [node for node in list(set(nodesToCheck) - set(remainNodes))]

            for node in remainNodes:
                if node in newNodes:
                    remainNodes.remove(node)
            
            for elem in data:
                labels = elem["labels"][0]
                properties = elem["properties"]
                if properties["uid"] in newNodes:
                    match labels:
                        case "Action":
                            action = Neo4j_Action.get_or_create({
                                "uid": properties["uid"],
                                "description": properties["description"],
                            })[0]
                            action.created_at = datetime.date.today()
                            action.save()
                        case "Activity":
                            activity = Neo4j_Activity.get_or_create({
                                "uid": properties["uid"],
                                "description": properties["description"]
                            })[0]
                            activity.created_at = datetime.date.today()
                            activity.save()
                        case "Actor":
                            actor = Neo4j_Actor.get_or_create({
                                "uid": properties["uid"],
                                "name": properties["name"]
                            })[0]
                            actor.created_at = datetime.date.today()
                            actor.save()
                        case "Environmental_Resource":
                            environmental_resource = Neo4j_Environmental_Resource.get_or_create({
                                "uid": properties["uid"],
                                "type": properties["type"],
                                "description": properties["description"],
                            })[0]
                            environmental_resource.created_at = datetime.date.today()
                            environmental_resource.save()
                        case "Objective":
                            objective = Neo4j_Objective.get_or_create({
                                "uid": properties["uid"],
                                "description": properties["description"]
                            })[0]
                            objective.created_at = datetime.date.today()
                            objective.save()
                        case "Performance":
                            performance = Neo4j_Performance.get_or_create({
                                "uid": properties["uid"],
                                "criterion": properties["criterion"],
                                "value": properties["value"],
                                "unit": properties["unit"]
                            })[0]
                            performance.created_at = datetime.date.today()
                            performance.save()
                        case "Personnal_Resource":
                            personnal_resource = Neo4j_Personnal_Resource.get_or_create({
                                "uid": properties["uid"],
                                "type": properties["type"],
                                "description": properties["description"]
                            })[0]
                            personnal_resource.created_at = datetime.date.today()
                            personnal_resource.save()
                        case "Scheme":
                            scheme = Neo4j_Scheme.get_or_create({
                                "uid": properties["uid"],
                                "actions_sequence": properties["actions_sequence"]
                            })[0]
                            scheme.created_at = datetime.date.today()
                            scheme.save()
                        case "Situation":
                            situation = Neo4j_Situation.get_or_create({
                                "uid": properties["uid"],
                                "description": properties["description"]
                            })[0]
                            situation.created_at = datetime.date.today()
                            situation.save()

                elif properties["uid"] in remainNodes:
                    match labels:
                        case "Proxyma_Corpus":
                            if proxyma.title != properties["title"]:
                                proxyma.title = properties["title"]
                                proxyma.save()
                        case "Action":
                            action = Neo4j_Action.nodes.get(uid=properties["uid"])
                            if action.description != properties["description"]:
                                action.description = properties["description"]
                                action.save()
                        case "Activity":
                            activity = Neo4j_Activity.nodes.get(uid=properties["uid"])
                            if activity.description != properties["description"]:
                                activity.description = properties["description"]
                                activity.save()
                        case "Actor":
                            actor = Neo4j_Actor.nodes.get(uid=properties["uid"])
                            if actor.name != properties["name"]:
                                actor.name = properties["name"]
                                actor.save()
                        case "Environmental_Resource":
                            environmental_resource = Neo4j_Environmental_Resource.nodes.get(uid=properties["uid"])
                            for key, value in properties.items():
                                if key != "uid" and key != "created_at" and getattr(environmental_resource, key) != value:
                                    setattr(environmental_resource, key, value)
                            environmental_resource.save()
                        case "Objective":
                            objective = Neo4j_Objective.nodes.get(uid=properties["uid"])
                            if objective.description != properties["description"]:
                                objective.description = properties["description"]
                                objective.save()
                        case "Performance":
                            performance = Neo4j_Performance.nodes.get(uid=properties["uid"])
                            for key, value in properties.items():
                                if key != "uid" and key != "created_at" and getattr(performance, key) != value:
                                    setattr(performance, key, value)
                            performance.save()
                        case "Personnal_Resource":
                            personnal_resource = Neo4j_Personnal_Resource.nodes.get(uid=properties["uid"])
                            for key, value in properties.items():
                                if key != "uid" and key != "created_at" and getattr(personnal_resource, key) != value:
                                    setattr(personnal_resource, key, value)
                            personnal_resource.save()
                        case "Scheme":
                            scheme = Neo4j_Scheme.nodes.get(uid=properties["uid"])
                            if scheme.actions_sequence != properties["actions_sequence"]:
                                scheme.actions_sequence = properties["actions_sequence"]
                                scheme.save()
                        case "Situation":
                            situation = Neo4j_Situation.nodes.get(uid=properties["uid"])
                            if situation.description != properties["description"]:
                                situation.description = properties["description"]
                                situation.save()                

            for node in nodesToDelete:
                node = db.cypher_query(f"MATCH (n{{uid:'{node}'}}) RETURN n;")[0][0][0]._properties
                node["deleted_at"] = datetime.date.today()

            for elem in data:
                labels = elem["labels"][0]
                properties = elem["properties"]
                relationships = elem["relationships"]
                if relationships is not None:
                    match labels:
                        case "Proxyma_Corpus":

                            db_actions = set([action.uid for action in proxyma.action.all()])
                            incoming_actions = set(relationships["actions"])
                            disconnect_actions = [proxyma.action.disconnect(Neo4j_Action.nodes.get(uid=action)) for action in list(db_actions - incoming_actions)]
                            connect_actions = [proxyma.action.connect(Neo4j_Action.nodes.get(uid=action)) for action in list(incoming_actions - db_actions)]

                            db_activities = set([activity.uid for activity in proxyma.activity.all()])
                            incoming_activities = set(relationships["activities"])
                            disconnect_activities = [proxyma.activity.disconnect(Neo4j_Activity.nodes.get(uid=activity)) for activity in list(db_activities - incoming_activities)]
                            connect_activities = [proxyma.activity.connect(Neo4j_Activity.nodes.get(uid=activity)) for activity in list(incoming_activities - db_activities)]

                            db_actors = set([actor.uid for actor in proxyma.actor.all()])
                            incoming_actors = set(relationships["actors"])
                            disconnect_actors = [proxyma.actor.disconnect(Neo4j_Actor.nodes.get(uid=actor)) for actor in list(db_actors - incoming_actors)]
                            connect_actors = [proxyma.actor.connect(Neo4j_Actor.nodes.get(uid=actor)) for actor in list(incoming_actors - db_actors)]

                            db_environmental_resources = set([environmental_resource.uid for environmental_resource in proxyma.environmental_resource.all()])
                            incoming_environmental_resource = set(relationships["environmental_resources"])
                            disconnect_environmental_resource = [proxyma.environmental_resource.disconnect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource)) for environmental_resource in list(db_environmental_resources - incoming_environmental_resource)]
                            connect_environmental_resource = [proxyma.environmental_resource.connect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource)) for environmental_resource in list(incoming_environmental_resource - db_environmental_resources)]

                            db_objectives = set([objective.uid for objective in proxyma.objective.all()])
                            incoming_objectives = set(relationships["objectives"])
                            disconnect_objectives = [proxyma.objective.disconnect(Neo4j_Objective.nodes.get(uid=objective)) for objective in list(db_objectives - incoming_objectives)]
                            connect_objectives = [proxyma.objective.connect(Neo4j_Objective.nodes.get(uid=objective)) for objective in list(incoming_objectives - db_objectives)]

                            db_performances = set([performance.uid for performance in proxyma.performance.all()])
                            incoming_performances = set(relationships["performances"])
                            disconnect_performances = [proxyma.performance.disconnect(Neo4j_Performance.nodes.get(uid=performance)) for performance in list(db_performances - incoming_performances)]
                            connect_performances = [proxyma.performance.connect(Neo4j_Performance.nodes.get(uid=performance)) for performance in list(incoming_performances - db_performances)]

                            db_personnal_resources = set([personnal_resource.uid for personnal_resource in proxyma.personnal_resource.all()])
                            incoming_personnal_resources = set(relationships["personnal_resources"])
                            disconnect_personnal_resources = [proxyma.personnal_resource.disconnect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource)) for personnal_resource in list(db_personnal_resources - incoming_personnal_resources)]
                            connect_personnal_resources = [proxyma.personnal_resource.connect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource)) for personnal_resource in list(incoming_personnal_resources - db_personnal_resources)]

                            db_schemes = set([scheme.uid for scheme in proxyma.scheme.all()])
                            incoming_schemes = set(relationships["schemes"])
                            disconnect_schemes = [proxyma.scheme.disconnect(Neo4j_Scheme.nodes.get(uid=scheme)) for scheme in list(db_schemes - incoming_schemes)]
                            connect_schemes = [proxyma.scheme.connect(Neo4j_Scheme.nodes.get(uid=scheme)) for scheme in list(incoming_schemes - db_schemes)]

                            db_situations = set([situation.uid for situation in proxyma.situation.all()])
                            incoming_situations = set(relationships["situations"])
                            disconnect_situations = [proxyma.situation.disconnect(Neo4j_Situation.nodes.get(uid=situation)) for situation in list(db_situations - incoming_situations)]
                            connect_situations = [proxyma.situation.connect(Neo4j_Situation.nodes.get(uid=situation)) for situation in list(incoming_situations - db_situations)]

                        case "Activity":
                            activity = Neo4j_Activity.nodes.get(uid=properties["uid"])

                            db_objectives = set([objective.uid for objective in activity.objective.all()])
                            incoming_objectives = set(relationships["objectives"])
                            disconnect_objectives = [activity.objective.disconnect(Neo4j_Objective.nodes.get(uid=objective)) for objective in list(db_objectives - incoming_objectives)]
                            connect_objectives = [activity.objective.connect(Neo4j_Objective.nodes.get(uid=objective)) for objective in list(incoming_objectives - db_objectives)]

                            db_schemes = set([scheme.uid for scheme in activity.scheme.all()])
                            incoming_schemes = set(relationships["schemes"])
                            disconnect_schemes = [activity.scheme.disconnect(Neo4j_Scheme.nodes.get(uid=scheme)) for scheme in list(db_schemes - incoming_schemes)]
                            connect_schemes = [activity.scheme.connect(Neo4j_Scheme.nodes.get(uid=scheme)) for scheme in list(incoming_schemes - db_schemes)]

                            db_situations = set([situation.uid for situation in activity.situation.all()])
                            incoming_situations = set(relationships["situations"])
                            disconnect_situations = [activity.situation.disconnect(Neo4j_Situation.nodes.get(uid=situation)) for situation in list(db_situations - incoming_situations)]
                            connect_situations = [activity.situation.connect(Neo4j_Situation.nodes.get(uid=situation)) for situation in list(incoming_situations - db_situations)]

                        case "Actor":
                            actor = Neo4j_Actor.nodes.get(uid=properties["uid"])

                            db_personnal_resources = set([personnal_resource.uid for personnal_resource in actor.personnal_resource.all()])
                            incoming_personnal_resources = set(relationships["personnal_resources"])
                            disconnect_personnal_resources = [actor.personnal_resource.disconnect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource)) for personnal_resource in list(db_personnal_resources - incoming_personnal_resources)]
                            connect_personnal_resources = [actor.personnal_resource.connect(Neo4j_Personnal_Resource.nodes.get(uid=personnal_resource)) for personnal_resource in list(incoming_personnal_resources - db_personnal_resources)]

                        case "Objective":
                            objective = Neo4j_Objective.nodes.get(uid=properties["uid"])
                            
                            db_performances = set([performance.uid for performance in objective.performance.all()])
                            incoming_performances = set(relationships["performances"])
                            disconnect_performances = [objective.performance.disconnect(Neo4j_Performance.nodes.get(uid=performance)) for performance in list(db_performances - incoming_performances)]
                            connect_performances = [objective.performance.connect(Neo4j_Performance.nodes.get(uid=performance)) for performance in list(incoming_performances - db_performances)]

                        case "Scheme":
                            scheme = Neo4j_Scheme.nodes.get(uid=properties["uid"])
                            
                            db_actions = set([action.uid for action in scheme.action.all()])
                            incoming_actions = set(relationships["actions"])
                            disconnect_actions = [scheme.action.disconnect(Neo4j_Action.nodes.get(uid=action)) for action in list(db_actions - incoming_actions)]
                            connect_actions = [scheme.action.connect(Neo4j_Action.nodes.get(uid=action)) for action in list(incoming_actions - db_actions)]

                            db_actors = set([actor.uid for actor in scheme.actor.all()])
                            incoming_actors = set(relationships["actors"])
                            disconnect_actors = [scheme.actor.disconnect(Neo4j_Actor.nodes.get(uid=actor)) for actor in list(db_actors - incoming_actors)]
                            connect_actors = [scheme.actor.connect(Neo4j_Actor.nodes.get(uid=actor)) for actor in list(incoming_actors - db_actors)]

                            db_environmental_resources = set([environmental_resource.uid for environmental_resource in scheme.environmental_resource.all()])
                            incoming_environmental_resource = set(relationships["environmental_resources"])
                            disconnect_environmental_resource = [scheme.environmental_resource.disconnect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource)) for environmental_resource in list(db_environmental_resources - incoming_environmental_resource)]
                            connect_environmental_resource = [scheme.environmental_resource.connect(Neo4j_Environmental_Resource.nodes.get(uid=environmental_resource)) for environmental_resource in list(incoming_environmental_resource - db_environmental_resources)]


        user_notify(request=self.request, user=self.request.user, message=f"OK_<{title}>", subject="Enregistrement des données terminé", template="data_register_confirmation_email.html")