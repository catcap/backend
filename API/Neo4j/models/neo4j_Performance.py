import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, DateProperty, BooleanProperty)

class Neo4j_Performance(StructuredNode):
	uid = UniqueIdProperty()
	criterion = StringProperty()
	value = StringProperty()
	unit = StringProperty()
	background = StringProperty(default="#F4A09F")
	highlight = StringProperty(default="#FF0200")
	created_at = DateProperty(default=datetime.date.today())
	deleted_at = DateProperty()

	def to_json(self):
		return {
			"labels": [
				"Performance"
			],
			"properties": {
				"uid": self.uid,
				"criterion": self.criterion,
				"value": self.value,
				"unit": self.unit,
                "background": self.background,
                "highlight": self.highlight,
				"created_at": self.created_at,
				"deleted_at": self.deleted_at,
			},
			"relationships": {
			}
		}