import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, DateProperty, BooleanProperty)

class Neo4j_Action(StructuredNode):
    uid = UniqueIdProperty()
    description = StringProperty()
    background = StringProperty(default="#AAAAAA")
    highlight = StringProperty(default="#FFFFFF")
    created_at = DateProperty(default=datetime.date.today())
    deleted_at = DateProperty()

    def to_json(self):
        return {
            "labels": [
                "Action"
            ],
            "properties": {
                "uid": self.uid,
                "background": self.background,
                "highlight": self.highlight,
                "description": self.description,
                "created_at": self.created_at,
                "deleted_at": self.deleted_at,
            },
            "relationships": {
            }
        }