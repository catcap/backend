import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, DateProperty, BooleanProperty)


class Neo4j_Environmental_Resource(StructuredNode):
	uid = UniqueIdProperty()
	type = StringProperty()
	description = StringProperty()
	background = StringProperty(default="#6EC667")
	highlight = StringProperty(default="#11FF00")
	created_at = DateProperty(default=datetime.date.today())
	deleted_at = DateProperty()
	
	def to_json(self):
		return {
			"labels": [
				"Environmental_Resource"
			],
			"properties": {
				"uid": self.uid,
				"type": self.type,
				"description": self.description,
                "background": self.background,
                "highlight": self.highlight,
				"created_at": self.created_at,
				"deleted_at": self.deleted_at,
			},
			"relationships": {
			}
		}