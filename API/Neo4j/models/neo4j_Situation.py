import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, DateProperty, BooleanProperty)

class Neo4j_Situation(StructuredNode):
	uid = UniqueIdProperty()
	description = StringProperty()
	background = StringProperty(default="#D9D9D9")
	highlight = StringProperty(default="#F0F0F0")
	created_at = DateProperty(default=datetime.date.today())
	deleted_at = DateProperty()
	
	def to_json(self):
		return {
			"labels": [
				"Situation"
			],
			"properties": {
				"uid": self.uid,
				"description": self.description,
                "background": self.background,
                "highlight": self.highlight,
				"created_at": self.created_at,
				"deleted_at": self.deleted_at,
			},
			"relationships": {
			}
		}