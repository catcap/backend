import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, Relationship, DateProperty, BooleanProperty)

from Neo4j.models.neo4j_Personnal_Resource import Neo4j_Personnal_Resource

class Neo4j_Actor(StructuredNode):
	uid = UniqueIdProperty()
	name = StringProperty()
	background = StringProperty(default="#FABC1C")
	highlight = StringProperty(default="#FFB700")
	created_at = DateProperty(default=datetime.date.today())
	deleted_at = DateProperty()

	# Relations
	personnal_resource = Relationship("Neo4j_Personnal_Resource", "OWNS")

	def to_json(self):
		personnal_resources = [personnal_resource.uid for personnal_resource in self.personnal_resource.all()]
		return {
			"labels": [
				"Actor"
			],
			"properties": {
				"uid": self.uid,
				"name": self.name,
                "background": self.background,
                "highlight": self.highlight,
				"created_at": self.created_at,
				"deleted_at": self.deleted_at,
			},
			"relationships": {
				"personnal_resources": personnal_resources,
			}
		}