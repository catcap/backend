import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, Relationship, DateProperty, BooleanProperty)

from Neo4j.models.neo4j_Performance import Neo4j_Performance

class Neo4j_Objective(StructuredNode):
	uid = UniqueIdProperty()
	description = StringProperty()
	background = StringProperty(default="#E95615")
	highlight = StringProperty(default="#FF5000")
	created_at = DateProperty(default=datetime.date.today())
	deleted_at = DateProperty()

	#Relations
	performance = Relationship("Neo4j_Performance", "EVALUATES")


	def to_json(self):
		performances = [performance.uid for performance in self.performance.all()]

		return {
			"labels": [
				"Objective"
			],
			"properties": {
				"uid": self.uid,
				"description": self.description,
                "background": self.background,
                "highlight": self.highlight,
				"created_at": self.created_at,
				"deleted_at": self.deleted_at,
			},
			"relationships": {
				"performances": performances,
			}
		}