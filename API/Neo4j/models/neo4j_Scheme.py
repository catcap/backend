import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, Relationship, DateProperty, BooleanProperty)

from Neo4j.models.neo4j_Action import Neo4j_Action
from Neo4j.models.neo4j_Actor import Neo4j_Actor
from Neo4j.models.neo4j_Environmental_Resource import Neo4j_Environmental_Resource

class Neo4j_Scheme(StructuredNode):
	uid = UniqueIdProperty()
	highlight = StringProperty(default="#00CCFF")
	background = StringProperty(default="#57C7E3")
	actions_sequence = StringProperty()
	created_at = DateProperty(default=datetime.date.today())
	deleted_at = DateProperty()

	# Relations
	action = Relationship("Neo4j_Action", "REFERENCES")
	actor = Relationship("Neo4j_Actor", "INVOLVES")
	environmental_resource = Relationship("Neo4j_Environmental_Resource", "MOBILISES")

	def to_json(self):
		actions = [action.uid for action in self.action.all()]
		actors = [actor.uid for actor in self.actor.all()]
		environmental_resources = [environmental_resource.uid for environmental_resource in self.environmental_resource.all()]
		return {
			"labels": [
				"Scheme"
			],
			"properties": {
				"uid": self.uid,
				"actions_sequence": self.actions_sequence,
                "background": self.background,
                "highlight": self.highlight,
				"created_at": self.created_at,
				"deleted_at": self.deleted_at,
			},
			"relationships": {
				"actions": actions,
				"actors": actors,
				"environmental_resources": environmental_resources,
			}
		}