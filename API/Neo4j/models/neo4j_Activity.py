import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, Relationship, DateTimeProperty, DateProperty, BooleanProperty)

from Neo4j.models.neo4j_Objective import Neo4j_Objective
from Neo4j.models.neo4j_Scheme import Neo4j_Scheme
from Neo4j.models.neo4j_Situation import Neo4j_Situation

class Neo4j_Activity(StructuredNode):
	uid = UniqueIdProperty()
	description = StringProperty()
	background = StringProperty(default="#549CDD")
	highlight = StringProperty(default="#0087FF")
	created_at = DateProperty(default=datetime.date.today())
	deleted_at = DateProperty()

	# Relations
	objective = Relationship("Neo4j_Objective", "CONTRIBUTES")
	scheme = Relationship("Neo4j_Scheme", "INVOLVES")
	situation = Relationship("Neo4j_Situation", "DEPENDS_ON")


	def to_json(self):
		objectives = [objective.uid for objective in self.objective.all()]
		schemes = [scheme.uid for scheme in self.scheme.all()]
		situations = [situation.uid for situation in self.situation.all()]
		return {
			"labels": [
				"Activity"
			],
			"properties": {
				"uid": self.uid,
				"description": self.description,
                "background": self.background,
                "highlight": self.highlight,
				"created_at": self.created_at,
				"deleted_at": self.deleted_at,
			},
			"relationships": {
				"objectives": objectives,
				"schemes": schemes,
				"situations": situations,
			}
		}