from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, DateTimeProperty, BooleanProperty, Relationship)

from Neo4j.models.neo4j_Action import Neo4j_Action
from Neo4j.models.neo4j_Activity import Neo4j_Activity
from Neo4j.models.neo4j_Actor import Neo4j_Actor
from Neo4j.models.neo4j_Environmental_Resource import Neo4j_Environmental_Resource
from Neo4j.models.neo4j_Objective import Neo4j_Objective
from Neo4j.models.neo4j_Performance import Neo4j_Performance
from Neo4j.models.neo4j_Personnal_Resource import Neo4j_Personnal_Resource
from Neo4j.models.neo4j_Scheme import Neo4j_Scheme
from Neo4j.models.neo4j_Situation import Neo4j_Situation

class Neo4j_Proxyma_Corpus(StructuredNode):
    uid = UniqueIdProperty()
    title = StringProperty()
    corpus_timestamp = DateTimeProperty(default_now=True)
    group = StringProperty()
    user = StringProperty()
    delete_mark = BooleanProperty(default=False)
    validated = BooleanProperty(default=True)

    # Relations
    action = Relationship("Neo4j_Action", "ENCAPSULATES")
    activity = Relationship("Neo4j_Activity", "ENCAPSULATES")
    actor = Relationship("Neo4j_Actor", "ENCAPSULATES")
    environmental_resource = Relationship("Neo4j_Environmental_Resource", "ENCAPSULATES")
    objective = Relationship("Neo4j_Objective", "ENCAPSULATES")
    performance = Relationship("Neo4j_Performance", "ENCAPSULATES")
    personnal_resource = Relationship("Neo4j_Personnal_Resource", "ENCAPSULATES")
    scheme = Relationship("Neo4j_Scheme", "ENCAPSULATES")
    situation = Relationship("Neo4j_Situation", "ENCAPSULATES")

    def to_json(self):
        actions = [action.uid for action in self.action.all()]
        activities = [activity.uid for activity in self.activity.all()]
        actors = [actor.uid for actor in self.actor.all()]
        environmental_resources = [environmental_resource.uid for environmental_resource in self.environmental_resource.all()]
        objectives = [objective.uid for objective in self.objective.all()]
        performances = [performance.uid for performance in self.performance.all()]
        personnal_resources = [personnal_resource.uid for personnal_resource in self.personnal_resource.all()]
        schemes = [scheme.uid for scheme in self.scheme.all()]
        situations = [situation.uid for situation in self.situation.all()]
        return {
            "labels": [
                "Proxyma_Corpus"
            ],
            "properties": {
                "uid": self.uid,
                "title": self.title,
            },
            "relationships": {
                "actions": actions,
                "activities": activities,
                "actors": actors,
                "environmental_resources": environmental_resources,
                "objectives": objectives,
                "performances": performances,
                "personnal_resources": personnal_resources,
                "schemes": schemes,
                "situations": situations,
            }
        }
