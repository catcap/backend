import datetime
from neomodel import (StructuredNode, UniqueIdProperty, StringProperty, DateProperty, BooleanProperty)

class Neo4j_Personnal_Resource(StructuredNode):
	uid = UniqueIdProperty()
	type = StringProperty(choices={'k': 'knowledge', 's': 'skill', 'a': 'ability'})
	description = StringProperty()
	background = StringProperty(default="#F79767")
	highlight = StringProperty(default="#FF97AA")
	created_at = DateProperty(default=datetime.date.today())
	deleted_at = DateProperty()

	def to_json(self):
		return {
			"labels": [
				"Personnal_Resource"
			],
			"properties": {
				"uid": self.uid,
				"type": self.type,
				"description": self.description,
                "background": self.background,
                "highlight": self.highlight,
				"created_at": self.created_at,
				"deleted_at": self.deleted_at,
			},
			"relationships": {
			}
		}